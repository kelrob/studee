<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlans;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPlans::truncate();
        $data = [
            [
                "name" => "Basic",
                "weekly_reads" => 5,
                "cost" => 10000,
                "downloads" => 10,
                "books_notification" => false,
                "created_at" => now()
            ],
            [
                "name" => "Starter",
                "weekly_reads" => 15,
                "cost" => 30000,
                "downloads" => 20,
                "books_notification" => false,
                "created_at" => now()
            ],

            [
                "name" => "Professional",
                "weekly_reads" => 0,
                "cost" => 50000,
                "downloads" => 0,
                "books_notification" => true,
                "created_at" => now()
            ],
        ];


        SubscriptionPlans::insert(
            $data
        );
    }
}
