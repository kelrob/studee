<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->words(5,true),
            'about' => $this->faker->sentence(20),
            'cover' => '/assets/images/books/1.jpg',
            'published_at' => now()->subYears(12),
            'author' =>$this->faker->name(),
        ];
    }
}
