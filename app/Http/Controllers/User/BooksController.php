<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Search;
use App\Interfaces\BookInterface;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    private $interface;

    public function __construct(BookInterface $interface)
    {
        $this->interface = $interface;
    }

    /**
     * Get All books
     * @param String $genre
     */
    public function index($genre = null)
    {
        try {
            $books = $this->interface->getBooks($genre);
            return view('users.books.index', $books);
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    /**
     * Get Book
     * @param String $id
     */
    public function get($book_id)
    {
        try {
            $book = $this->interface->getBook(decrypt($book_id), false);
            return view('users.books.book', $book);
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    /**
     * Search for books
     * @param Search $request
     */
    public function search(Search $request)
    {
        $results = $this->interface->search($request);
        return view('users.books.index', [
            'title' => 'Result for ' . $request->input,
            'books' => $results,
        ]);
    }

    /**
     * Read Book
     * @param String $id
     */
    public function readBook($book)
    {
        try {
            $readBook = $this->interface->readBook(decrypt($book));
            if ($readBook['error'] == false) {
                return view('users.books.read', $readBook);
            }
            session()->flash('error', 'Subscription limit exceeded please upgrade your plan to continue');
            return redirect()->route('subscription-plan');
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    public function add($book_id)
    {
        try {
            $addBook = $this->interface->addToLibrary($book_id);
            if ($addBook) {
                session()->flash('success', 'Book was added successfully');
                return redirect()->route('myLibrary');
            } else {
                session()->flash('error', 'Please subscribe to perform this action');
                return redirect()->route('subscription-plan');
            }
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    public function download($book_id)
    {
        try {
            $addBook = $this->interface->downloadBook($book_id);
            session()->flash('success', 'Book was added successfully');
            return redirect()->route('myLibrary');
        } catch (\Throwable$err) {
            return abort(503);
        }
    }
}
