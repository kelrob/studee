<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Interfaces\PageInterface;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    private $interface;
    public function __construct(PageInterface $interface)
    {
        $this->interface  = $interface;
    }


    public function welcome()
    {
        try {
            $data = $this->interface->welcome();
            return view('welcome', $data);
        } catch (\Throwable $th) {
            abort(503);
        }
    }


    /**
     * Get dashboard data
     * @return Response
     */
    public function dashboard()
    {
        try {
            $data = $this->interface->userDashboard();
            return view('dashboard', $data);
        } catch (\Throwable $err) {
           abort(503);
        }
        
    }


    /**
     * Get library data
     * @return Response
     */
    public function myLibrary()
    {
        try {
            $data = $this->interface->library();
            return view('users.library', $data);
        } catch (\Throwable $err) {
            abort(503);
        }
    }

}
