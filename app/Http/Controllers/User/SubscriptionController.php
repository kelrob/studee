<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Subscribe;
use App\Interfaces\SubscriptionInterface;
use App\Jobs\SendSubscriptionNotification;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    private $interface;

    public function __construct(SubscriptionInterface $interface)
    {
        $this->interface = $interface;
    }

    /**
     * Get all subscription plans
     */
    public function plans()
    {
        try {
            $plans = $this->interface->getPlans();
            return view('users.subscription.plans', [
                'plans' => $plans,
            ]);
        } catch (\Throwable$err) {
            abort(503);
        }
    }

    /**
     * subscribe to a plan
     * @param Subscribe $request
     */
    public function choosePlan(Subscribe $request)
    {
        try {
            $subscribe = $this->interface->subscribe($request);
            if ($subscribe['error'] == true) {
                session()->flash('error', 'failed to initiate payment');
                return redirect()->back();
            }
            return redirect($subscribe['link']);

        } catch (\Throwable$err) {
            abort(503);
        }
    }

    public function callback()
    {

        // dd('Hello');

        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event = json_decode($input);
        // Do something with $event
        // http_response_code(200);

        // parse event (which is json string) as object
        // Do something - that will not take long - with $event

        $reference = $_GET['reference'];
        $trxref = $_GET['trxref'];

        $subscription = Subscription::where('payment_ref', $reference)->firstOrFail();

        if ($subscription) {
            $verify = verify_payment($reference);

            if ($verify['status'] == true && $verify['data'] != null) {
                $subscription->update([
                    'confirmed' => true,
                    'end_date' => Carbon::now()->addDays(30),
                ]);
            }
        }

        $event = json_decode($input);

        SendSubscriptionNotification::dispatch($subscription);

        session()->flash('success', 'Subscribed successful');
        return redirect()->route('dashboard');
    }
}
