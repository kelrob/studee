<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Password;
use App\Http\Requests\Profile;
use App\Interfaces\UserInterface;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    private $interface; 


    public function __construct(UserInterface $interface)
    {
       $this->interface = $interface;
    }


    /**
     * Change Current Password
     * @param Profile $request
     */
    public function changePassword(Password $request)
    {
        try {
            $updatePassword = $this->interface->changePassword($request);

            if($updatePassword){
                session()->flash('success', 'Password Updated Successfully');
            }
            session()->flash('error', 'incorrect password');
            return redirect()->route('profile');
        } catch (\Throwable $th) {
            abort(503);
        }
    }


    /**
     * show shange password form
     * @param 
     */
    public function password()
    {
        return view('users.password');
    }



    /**
     * Update user profile
     * @param Profile $request
     */
    public function updateProfile(Profile $request)
    {
        try {
            $updatedProfile = $this->interface->updateProfile($request);

            if ($updatedProfile) {
                session()->flash('success', 'Profile Updated Successfully');
            }
            session()->flash('error', 'something went wrong');

            return redirect()->route('profile');
        } catch (\Throwable $th) {
            abort(503);
        }
    }

    /**
     * Show profile Page
     */
    public function index()
    {
        try {
            return view('users.profile');
        } catch (\Throwable $th) {
            abort(503);
        }
    }
}
