<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Plan;
use App\Interfaces\SubscriptionInterface;
use Illuminate\Http\Request;

class Plans extends Controller
{
    private $interface;

    public function __construct(SubscriptionInterface $_interface)
    {
        $this->interface = $_interface;
    }

    /**
     * List subscription Plans
     */
    public function index()
    {
        try {
            $plans = $this->interface->getPlans();
            return view('admin.subscription.plans', [
                'plans' => $plans,
            ]);
        } catch (\Throwable $th) {
            return abort(503);
        }
    }


    /**
     * Edit / View plan
     * @param String $plan
     */
    public function edit($plan)
    {
        try {
            $plan = $this->interface->getPlan(decrypt($plan));
            return view('admin.subscription.plan', [
                'plan' => $plan
            ]);
        } catch (\Throwable $th) {
            return abort(503);
        }
    }


    /**
     * Update Subscription Plan Configuration
     * @param Plan $request
     */
    public function updatePlan(Plan $request)
    {
        try {
            $updatePlan = $this->interface->updatePlan($request);
            
            session()->flash('success', 'Plan was updated successfully');

            return redirect()->route('admin.plans');
        } catch (\Throwable $th) {
            return abort(503);
        }
    }
    
}
