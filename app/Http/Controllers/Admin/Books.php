<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests\AddGenre;
use App\Interfaces\BookInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddBook;
use App\Http\Requests\Admin\EditGenre;
use App\Http\Requests\Admin\UpdateBook;

class Books extends Controller
{
    private $interface;

    public function __construct(BookInterface $_interface)
    {
        $this->interface = $_interface;
    }

    /**
     * get all books
     */
    public function index()
    {
        try {
            $books = $this->interface->getBooks();
            return view('admin.books.list', $books);
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    /**
     * Get book
     * @param Strung $book_id
     */
    public function book($book_id)
    {
        try {
            $book = $this->interface->getBook(decrypt($book_id));
            return view('admin.books.book', $book);
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    public function add()
    {
        try {
            $genres = $this->interface->getGenres();
            return view('admin.books.add', [
                'genres' => $genres,
            ]);
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    public function save(AddBook $request)
    {
        try {
            $save = $this->interface->addBook($request);
            session()->flash('success', 'Book added successful');
            return redirect()->route('admin.books');

        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    /**
     * Show edit page
     */
    public function edit($book_id)
    {
        try {
            $book = $this->interface->getBook(decrypt($book_id));
            return view('admin.books.book', $book);
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    /**
     * Update book
     * @param UpdateBook $request
     */
    public function update(UpdateBook $request)
    {
        try {
            $update = $this->interface->updateBook($request);
            session()->flash($update ? 'success' : 'error', $update ? 'Book updated successfully' : 'Error while updating books');
            return redirect()->route('admin.books');
        } catch (\Throwable$err) {
            return abort(503);
        }
    }

    /**
     * List genres
     */
    public function genres()
    {
        $genres = $this->interface->getGenres();
        return view('admin.genre.list', [
            'genres' => $genres,
        ]);
    }


    /**
     * List All genres
     */
    public function genre(AddGenre $request)
    {
        try {
            $genre = $this->interface->addGenre($request);
            session()->flash('success', 'Genre was added successfully');
            return redirect()->route('admin.genres');
        } catch (\Throwable $th) {
            return abort(503);
        }
    }


    /**
     * Show edit view for genre
     */
    public function editGenre($genre_id)
    {
        try {
            $genre = $this->interface->getGenre($genre_id);
            return view('admin.genre.edit', [
                'genre' =>  $genre
            ]);
        } catch (\Throwable $th) {
           return abort(503);
        }
    }


    public function updateGenre(EditGenre $request)
    {
        try {
            $update = $this->interface->editGenre($request);
            session()->flash('success', 'genre was update successfully');
            return redirect()->route('admin.genres');
        } catch (\Throwable $th) {
            return abort(503);
        }
    }


    /**
     * Delete Genre
     */
    public function deleteGenre($genre_id)
    {
        try {
            $delete = $this->interface->deleteGenre($genre_id);
            session()->flash('success', "genre was deleted successfully");

            return redirect()->route('admin.genres');

        } catch (\Throwable $th) {
            return abort(503);
        }
    }
}
