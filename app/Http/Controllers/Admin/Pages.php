<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\PageInterface;
use Illuminate\Http\Request;

class Pages extends Controller
{

    private $interface;

    public function __construct(PageInterface $interface)
    {
        $this->interface = $interface;
    }

    /**
     * Admin Dashboard
     */
    public function index()
    {
       try {
           $data = $this->interface->adminDashboard();
           return view('admin.index', $data);
       } catch (\Throwable $err) {
           return abort(503);
       }
    }


    /**
     * Get user
     */
    public function user($user)
    {
        try {
            $id = decrypt($user);
            $user = $this->interface->getUser($id);
            return view('admin.users.user', $user);
        } catch (\Throwable $th) {
            return abort(503);
        }
    }


    /**
     * Get Users
     */
    public function users($category = null)
    {
        try {
            $users = $this->interface->getUsers($category);
            return view('admin.users.list', [
                'users' => $users,
                'title' => ucfirst($category) .' Users',
            ]);
        } catch (\Throwable $th) {
            return abort(503);
        }
    }



}
