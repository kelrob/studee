<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

class OverwriteController extends Controller
{

    /**
     * Verify Email via link
     */
    public function verifyEmail(EmailVerificationRequest $request)
    {
        $request->fulfill();
        return redirect()->route('dashboard');
    }

    /**
     * Verification Page
     */
     public function openVerificationPage()
     {
         return view('auth.verify-email');
     }
}
