<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:200',
            'description' => 'required',
            'cover' => 'required|mimes:png,jpg,svg',
            'author' => 'required',
            'genre' => 'nullable',
            'publish_date' => 'required',
            'book' => 'required',
        ];
    }
}
