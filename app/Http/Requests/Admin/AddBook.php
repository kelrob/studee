<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:200',
            'author' => 'required',
            'genre' => 'nullable',
            'publish_date' => 'required',
            'description' => 'required',
            'cover' => 'required|mimes:png,jpg,jpeg,svg',
            // 'page_title.*' => 'required',
            // 'page_file.*' => 'required|file|mimes:pdf',
            'file' => 'required|file|mimes:pdf'
        ];
    }
}
