<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Plan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan' => 'required',
            'name' => 'required',
            'cost' => 'required|numeric',
            'downloads' => 'required|numeric',
            'reads' => 'required|numeric',
            'notifiable' => 'required|boolean',
        ];
    }
}
