<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;

class PhotoUpload extends Component
{
    use WithFileUploads;

    public $image;


    public function upload_photo()
    {
        $user = auth()->user();
        $user->updateProfilePhoto($this->image);
        session()->flash('success', 'Profile photo was updated successfully');
        return redirect()->route('profile');
    }

    
    public function render()
    {
        return view('livewire.photo-upload');
    }
}
