<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\App;
use Livewire\Component;

class ReadBook extends Component
{
    public $book, $pages, $page;

    public function mount($book)
    {
        $this->book = $book;
        $this->pages = $book->pages;
    }


    public function selectPage($page)
    {
        // $pdf = App::loadView(['view' , $page->path]);
        // return response()->streamDownload(function () {
        //     echo 'CSV Contents...';
        // }, 'export.csv');

        $this->page = $page;
    }

    public function render()
    {
        return view('livewire.read-book');
    }
}
