<?php

namespace App\Http\Livewire;

use App\Models\Genre;
use Livewire\Component;

class AddBook extends Component
{
    public $genres;

    public $rules = [

        'title' => 'required|min:200',
        'description' => 'required',
        'cover' => 'required|mimes:png,jpg,svg|size:5',
        'author' => 'required',
        'genre' => 'nullable',
        'publish_date' => 'required|date_format:d-m-y',
        'file' => 'required|file',
        // 'page_title.*' => 'required',
        // 'page_file.*' => 'required|file|mimes:pdf',
    ];
    public $inputs = [];
    public $i = 1;


    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs, $i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    private function resetInputFields()
    {
        $this->page_title = '';
        $this->page_file = '';
    }

    public function mount()
    {
        $this->genres = Genre::orderBy('name', 'asc')->get();
    }


    public function render()
    {
        return view('livewire.add-book');
    }
}
