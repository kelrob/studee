<?php

namespace App\Http\Livewire;

use App\Models\Genre;
use Livewire\Component;

class EditBook extends Component
{
    public $book, $genres;

    public $rules = [
        'title' => 'required|min:200',
        'description' => 'required',
        'cover' => 'required|mimes:png,jpg,svg|size:5',
        'author' => 'required',
        'genre' => 'nullable',
        'publish_date' => 'required|date_format:d-m-y',
        'book' => 'required',
    ];

    public $input = [];
    

    public function mount($book)
    {
        $this->book = $book;
        $this->genres = Genre::orderBy('name', 'asc')->get();
    }


    public function render()
    {
        return view('livewire.edit-book');
    }
}
