<?php

namespace App\Http\Livewire;

use App\Models\Genre;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddGenre extends Component
{
    use WithFileUploads;

    public $rules = [
        'name' => 'required',
        'image' => 'required|mimes:jpg,png,svg|size:5042'
    ];

    public $name, $image;


    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    public function addGenre()
    {
        $this->validate();

        Genre::create([
            'name' => $this->name,
            'image' => uploadFile($this->image, '/books/genre/', true)
        ]);

        
        session()->flash('success', 'Genre was added successfully');
        return redirect()->route('admin.genres');
    }
    public function render()
    {
        return view('livewire.add-genre');
    }
}
