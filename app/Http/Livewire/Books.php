<?php

namespace App\Http\Livewire;

use App\Models\Book;
use App\Models\Genre;
use App\Models\Library;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Books extends Component
{
    public $books, $genres, $genre, $recentlyPublished, $topPicks, $getGenre;


    public function getRecentlyPublished()
    {
        $this->recentlyPublished = Book::orderBy('created_at', 'desc')->get()->take(4);
    }

    public function getTopPicks()
    {
        $getTrendingBooks = Library::orderBy('created_at')->get()->groupBy(function ($data) {
            return $data->book_id;
        });
        $topPicks = [];
        foreach ($getTrendingBooks as $key => $value) {
            if (count($topPicks) < 4) {
                $book = Book::where('id', $key)->first();
                $topPicks[] = $book;
            }
        }
        $this->topPicks = $topPicks;
    }

    

    public function mount($books, $genre = null)
    {
        $this->books = $books;
        $this->genres = Genre::orderBy("name", 'asc')->get();
        $this->genre = $genre;
        $this->getRecentlyPublished();
        $this->getTopPicks();
    }


    public function selectGenre($genre)
    {
        $this->getGenre = Genre::where('id', $genre)->first();
        $this->books = $this->getGenre->books;
    }
    

    public function render()
    {
        return view('livewire.books');
    }
}
