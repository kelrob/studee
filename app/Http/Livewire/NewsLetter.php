<?php

namespace App\Http\Livewire;

use App\Models\NewletterSubscriber;
use Livewire\Component;

class NewsLetter extends Component
{
    public $instance, $email;


    public $rules = [
        'email' => 'required|email'
    ];


    public function mount($instance)
    {
        $this->instance = $instance;
    }


    public function subscribe()
    {
        $this->validate();

        $subcribe = NewletterSubscriber::firstOrCreate([
            'email' => $this->email,
        ]);

        $this->email = '';

        session()->flash('success', 'Subscription successful');

    }


    public function render()
    {
        return view('livewire.news-letter');
    }
}
