<?php

namespace App\View\Components;

use App\Models\Genre;
use Illuminate\View\Component;

class GenreDropdown extends Component
{
    public $genres;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->genres = Genre::orderBy('name', 'asc')->get();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.genre-dropdown');
    }
}
