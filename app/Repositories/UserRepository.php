<?php

namespace App\Repositories;

use App\Interfaces\UserInterface;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserInterface{

    public function updateProfile($request)
    {
        $user = auth()->user();

        $user->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            // 'phone' => $request->phone,
            'is_male' => $request->gender,
            'about' => $request->about
        ]);
        

        return true;
    }



    public function changePassword($request)
    {
        $user = auth()->user();
        $checkPassword = Hash::check($request->old_password, $user->password);

        // if($checkPassword){
            $newPassword =  Hash::make($request->password);

            $user->update([
                'password' => $newPassword
            ]);

        //     return true;
        // }

        return false;
    }
}