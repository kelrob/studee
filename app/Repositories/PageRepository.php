<?php

namespace App\Repositories;

use App\Interfaces\PageInterface;
use App\Models\Book;
use App\Models\Download;
use App\Models\Genre;
use App\Models\Library;
use App\Models\User;

class PageRepository implements PageInterface
{

    public function userDashboard()
    {
        $user = auth()->user();
        $genres = Genre::orderBy('created_at', 'desc')->get();
        $getTrendingBooks = Library::orderBy('created_at')->get()->groupBy(function ($data) {
            return $data->book_id;
        });

        $trendingBooks = [];
        foreach ($getTrendingBooks as $key => $value) {
            $book = Book::where('id', $key)->first();
            $trendingBooks[] = $book;
        }

        $topPicks = [];
        foreach ($getTrendingBooks as $key => $value) {
            if (count($topPicks) < 5) {
                $book = Book::where('id', $key)->first();
                $topPicks[] = $book;
            }
        }

        $recentlyPublished = Book::orderBy('created_at', 'desc')->get()->take(5);
        return [
            'trendingBooks' => $trendingBooks,
            'genres' => $genres,
            'topPicks' => $topPicks,
            'recentlyPublished' => $recentlyPublished,
        ];
    }

    public function library()
    {
        $user = auth()->user();
        $suggestedBooks = Book::inRandomOrder()->orderBy('views', 'desc')->get();
        $openedBooks = $user->books()->where('is_completed', '=', false)->with('book')->get();
        $completedBooks = $user->books()->where('is_completed', true)->with('book')->get();
        $downloaded = $user->downloads()->with('book')->get();

        return [
            'suggestedBooks' => $suggestedBooks,
            'openedBooks' => $openedBooks,
            'completedBooks' => $completedBooks,
            'downloadedBooks' => $downloaded,
        ];
    }

    public function adminDashboard()
    {
        $user = auth()->user();
        $users = User::orderBy('created_at', 'desc')->get();
        $unsubscribed_users = 0;
        $subscribed_users = 0;

        $downloads = Download::orderBy('created_at', 'desc')->get();

        foreach ($users as $user) {
            if ($user->is_subscribed()) {
                $subscribed_users = $subscribed_users + 1;
            } else {
                $unsubscribed_users = $unsubscribed_users + 1;
            }
        }

        $books = Book::orderBy('created_at', 'desc')->get();
        $genre = Genre::orderBy('created_at', 'desc')->get();
        $topDownloads = $downloads->groupBy(function ($data) {
            return $data->book_id;
        });

        return [
            'users' => $users,
            'unsubscribed' => $unsubscribed_users,
            'subscribed' => $subscribed_users,
            'books' => $books,
            'downloads' => $downloads,
            'genres' => $genre,
        ];
    }

    public function getUser($user_id)
    {
        $user = User::where('id', $user_id)->first();
        $library = $user->books;
        $subscription = $user->subscriptions()->whereDate('end_date', '>', now())->first();
        $downloads = $user->downloads;
        return [
            'library' => $library,
            'user' => $user,
            'subscription' => $subscription,
            'downloads' => $downloads,
        ];
    }

    public function getUsers($category = null)
    {
        $users = User::all();
        $unsubscribed_users = [];
        $subscribed_users = [];

        if ($users->count() > 0) {
            foreach ($users as $user) {
                if ($user->is_subscribed()) {
                    $subscribed_users[] = $user;
                } else {
                    $unsubscribed_users[] = $user;
                }
            }
        }

        if ($category == null) {
            return $users;
        } elseif ($category == 'unsubscribed') {
            return $unsubscribed_users;
        }

        return $subscribed_users;
    }

    public function welcome()
    {
        return [
            'books' => Book::orderBy('created_at', 'desc')->get()->take(4),
        ];
    }
}
