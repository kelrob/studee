<?php

namespace App\Repositories;

use Carbon\Carbon;
use CaliCastle\Cuid;
use App\Models\Subscription;
use App\Models\SubscriptionPlans;
use App\Interfaces\SubscriptionInterface;

class SubscriptionRepository implements SubscriptionInterface{

    public function getPlans()
    {
        return SubscriptionPlans::all();
    }


    public function subscribe($request)
    {
        $plan = SubscriptionPlans::where('id', $request->plan)->first();
        $user = auth()->user();
        $amount = $plan->cost;
        $redirect = route('payment-callback');
        $code = Cuid::make('stp');
        $payment = payment($amount, $code, $redirect, $user);
        $status = $payment['status'] ==  true;

        $payment_link = $payment['link'];


        if ($status && $payment != null) {

            $createSubscription = Subscription::create([
                'user_id' => $user->id,
                'plan_id' => $plan->id,
                'payment_ref' => $payment['data']['reference'],
                'end_date' => Carbon::now()->addDays(30), 
            ]);

            return [
                'error' => false,
                'link' => $payment_link
            ];
        }

        return [
            'error' => true,
            'link' => null,
            'payment' => $payment
        ];
    }


    public function updatePlan($request)
    {
       $plan = SubscriptionPlans::where('id', $request->plan)->firstOrFail();
       $plan->update([
            'name' => ucfirst($request->name),
            'cost' => $request->cost,
            'downlaods' => $request->downloads,
            'wekly_reads' => $request->reads,
            'books_notification' => $request->notifiable,
       ]);

       return true;
    }


    public function getPlan($id)
    {
        return SubscriptionPlans::where('id', $id)->firstOrFail();
    }

}