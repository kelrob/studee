<?php

namespace App\Repositories;

use App\Interfaces\BookInterface;
use App\Models\Book;
use App\Models\Download;
use App\Models\Downloadable;
use App\Models\Genre;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelCrossEloquentSearch\Search;

class BookRepository implements BookInterface
{

    /**
     * Get all books
     */
    public function getBooks($genre = null)
    {
        if ($genre == null) {
            $books = Book::orderBy('created_at', 'desc')->get();
            return [
                'books' => $books,
            ];
        } else {
            $genre = Genre::where('name', $genre)->first();
            $books = $genre->books()->orderBy('created_at', 'desc')->get();
            return [
                'books' => $books,
                'genre' => $genre,
            ];
        }
    }

    public function getBook($id, $just_book = true)
    {
        $book = Book::with('downloadable')->whereId($id)->firstOrFail();
        $user = auth()->user();
        if ($just_book) {
            return [
                'book' => $book,
            ];
        }
        $inLibrary = $user->books()->where('book_id', $book->id)->first();
        return [
            'book' => $book,
            'inLibrary' => $inLibrary,
        ];
    }

    //Search Book
    public function search($request)
    {
        $input = $request->input;
        if (Book::count() > 0) {

            $result = Search::new ()->add(Book::class, 'title')
                ->add(Book::class, 'author')
                ->beginWithWildcard()
                ->get($input);
            return $result;
        }

        return null;

    }

    public function readBook($book_id)
    {
        $user = auth()->user();
        $subscribed = $user->is_subscribed();
        if ($subscribed) {
            $subscription = $user->subscriptions()->whereDate('end_date', '>', Carbon::today())->where('confirmed', true)->first();
            $subscriptionPlan = $subscription->plan;
            $book = $this->getBook($book_id)['book'];
            $pages = $book->pages;
            $inLibrary = $user->books()->where('book_id', $book->id)->first();
            if (!$inLibrary) {
                $library = $user->books()->whereDate('created_at', '>=', $subscription->created_at)->whereReading(true)->get();
                if ($subscriptionPlan->weekly_reads < 1 || $library->count() < $subscriptionPlan->weekly_reads * 4) {
                    $user->books()->create([
                        'book_id' => $book->id,
                    ]);
                    $book->update([
                        'views' => $book->views + 1,
                    ]);
                    return [
                        'error' => false,
                        'book' => $book,
                        'pages' => $pages,
                    ];
                }
            } elseif ($inLibrary && $inLibrary->reading == false) {
                $inLibrary->update([
                    'reading' => true,
                ]);
                return [
                    'error' => false,
                    'book' => $book,
                    'pages' => $pages,
                ];
            } else {
                return [
                    'error' => false,
                    'book' => $book,
                    'pages' => $pages,
                ];
            }

        }
        return [
            'error' => true,
        ];

    }

    public function addToLibrary($book_id)
    {
        $id = decrypt($book_id);
        $user = auth()->user();
        $subscribed = $user->is_subscribed();
        if ($subscribed) {
            $subscription = $user->subscriptions()->whereDate('end_date', '>', Carbon::today())->where('confirmed', true)->first();
            $subscriptionPlan = $subscription->plan;
            $book = $this->getBook($book_id)['book'];
            $inLibrary = $user->books()->where('book_id', $book->id)->first();
            if (!$inLibrary) {
                $user->books()->create([
                    'book_id' => $book->id,
                ]);
                return true;
            }
            return true;
        }

        return false;
    }

    public function downloadBook($book_id)
    {

        $user = auth()->user();
        $subscribed = $user->is_subscribed();
        if ($subscribed) {
            $book = $this->getBook(decrypt($book_id))['book'];
            $subscription = $user->subscriptions()->whereDate('end_date', '>', Carbon::today())
                ->where('confirmed', true)->first();
            $subscriptionPlan = $subscription->plan;
            $downloads = $user->downloads()->whereDate('created_at', '>=', $subscription->created_at)
                ->whereDate('created_at', '<=', $subscription->end_date)->get();

            if ($subscriptionPlan->downloads < 1 || $downloads->count() < $subscriptionPlan->downloads) {
                //Download Logic
                DB::transaction(function () use ($book, $user) {

                    $download = Download::create([
                        'user_id' => $user->id,
                        'book_id' => $book->id,
                    ]);

                    $downloadable = $book->downloadable;

                    $file = Storage::path($downloadable->path);

                    $headers = array('Content-Type' => 'application/octet-stream');
                    $zip_new_name = "$book->title-" . date("y-m-d-h-i-s") . ".zip";
                    return response()->download($file, $zip_new_name, $headers);
                });

            }

        }

        return [
            'error' => true,
        ];
    }

    public function updateBook($request)
    {

        $book = $this->getBook($request->book)['book'];

        $genre = $request->genre != null ?
        Genre::where('id', $request->genre)->first() :
        Genre::where('name', 'uncategorized')->first();

        $cover = uploadFile($request->cover, '/books/covers/', true);

        $book->update([
            'title' => $request->title,
            'author' => $request->author,
            'about' => $request->description,
            'cover' => $cover,
            'published_at' => now()->parse($request->publish_date),
            'genre_id' => $genre->id,
        ]);

        return true;

    }

    public function addBook($request)
    {

        $cover = '/books/cover.jpg';
        if ($request->hasFile('cover')) {
            $cover = uploadFile($request->cover, '/books/covers', true);
        }

        $genre = $request->genre != null ?
        Genre::where('id', $request->genre)->first() :
        Genre::where('name', 'uncategorized')->first();

        $book = Book::create([
            'title' => $request->title,
            'author' => $request->author,
            'about' => $request->description,
            'cover' => $cover,
            'published_at' => now()->parse($request->publish_date),
            'genre_id' => $genre->id,
        ]);

        $downloadable = Downloadable::create([
            'book_id' => $book->id,
            'path' => uploadFile($request->file, '/public/books'),
        ]);

        // foreach ($request->page_title as $key => $value) {
        //     $page = Page::create([
        //         'book_id' => $book->id,
        //         'title' => $request->page_title[$key],
        //         'file_path' => uploadFile($request->page_file[$key], '/books/pages'),
        //     ]);
        // }

        return true;
    }

    public function remove($book_id)
    {
        $book = $this->getBook($book_id)['book'];
        $book->delete();
        return true;
    }

    public function getGenres()
    {
        return Genre::orderBy('name', 'asc')->with('books')->get();
    }

    public function addGenre($request)
    {
        Genre::create([
            'name' => $request->name,
            'image' => uploadFile($request->image, '/books/genre/', true),
        ]);

        return true;
    }

    public function getGenre($genre_id)
    {
        return Genre::where('id', decrypt($genre_id))->firstOrFail();
    }

    public function editGenre($request)
    {
        $genre = $this->getGenre(encrypt($request->genre));
        $genre->update([
            'name' => $request->name,
            'image' => uploadFile($request->image, '/books/genre/', true),
        ]);

        return true;
    }

    public function deleteGenre($genre_id)
    {
        $genre = $this->getGenre($genre_id);
        $genre->delete();
        return true;
    }

}
