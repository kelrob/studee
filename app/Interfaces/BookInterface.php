<?php

namespace App\Interfaces;

interface BookInterface{

    public function getBooks($genre = null);

    public function getBook($id,$just_book = true);

    public function readBook($book_id);

    public function addToLibrary($book_id);

    public function search($request);

    public function getGenres();

    public function updateBook($request);
    
    public function addBook($request);

    public function addGenre($request);

    public function editGenre($request);

    public function getGenre($genre_id);

    public function deleteGenre($genre_id);

}