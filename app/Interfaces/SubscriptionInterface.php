<?php

namespace App\Interfaces;

interface SubscriptionInterface {

    public function getPlans();
    
    public function subscribe($request);

    public function updatePlan($request);

    public function getPlan($id);
    
}