<?php

namespace App\Interfaces;

interface PageInterface{

    public function welcome();
    
    public function userDashboard();

    public function library();

    public function adminDashboard();

    public function getUser($user_id);
    
    public function getUsers($category = null);
}