<?php
namespace App\Interfaces;

interface UserInterface {

    public function updateProfile($request);

    public function changePassword($request);
}