<?php

use CaliCastle\Cuid;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

if(!function_exists('truncateString')){
    function truncateString($text, $maxchar, $end = '...')
    {
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i      = 0;
            while (1) {
                $length = strlen($output) + strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                } else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        } else {
            $output = $text;
        }
        return $output;
    }
}


if(!function_exists(('payment'))){
    function payment($amount, $ref, $redirect, $customer)
    {
        $user = $customer;
        $amount = $amount * 100;

        try {
            
            $key = config('services.paystack.sk');
            $endpoint = config('services.paystack.url').'/transaction/initialize';
            $response = Http::withHeaders(['Authorization' => 'Bearer '. $key])->post($endpoint, [
                'reference' => $ref,
                'amount' => $amount,
                'currency' => "NGN",
                'callback_url' => $redirect,
                'email' => $user->email, 
            ])->json();

            $payment_link = $response['data']['authorization_url'];

            if($response['status'] == true){
                return [
                    'status' => true,
                    'link' => $payment_link,
                    'data' => $response['data']
                ];
            }else{
                return [
                    'status' => false,
                    'link' => null
                ];
            }
        } catch (\Throwable $th) {
            //throw $th;

            return [
                'status' => false,
                'link' => null,
                'message' => $th->getMessage()
            ];
        }

    }


    if(! function_exists('verify_payment')){
        function verify_payment($ref)
        {
            try {

                $key = config('services.paystack.sk');
                $endpoint = config('services.paystack.url') . '/transaction/verify/' . $ref;
                $response = Http::withHeaders(['Authorization' => 'Bearer ' . $key])->get($endpoint);

                if ($response['status'] == true) {
                    return [
                        'status' => $response['status'],
                        'data' => $response['data'],
                        'message' => $response['message']
                    ];
                } else {
                    return [
                        'status' => false,
                        'data' => null,
                        'message' => $response['message']
                    ];
                }
            } catch (\Throwable $err) {
                //throw $th;

                return [
                    'status' => false,
                    'data' => null,
                    'message' => $err->getMessage()
                ];
            }
        }
    }
}


if(!function_exists('uploadFile')){
    function uploadFile($file, $path, $public = false)
    {
        $name = Cuid::make('stpg');
        $extension = $file->getClientOriginalExtension();
        $fileName = $name . '.' . $extension;
        $upload = '';
        if($public){
            $file->move(public_path($path), $fileName);
            
            $upload = $path .'/'.$fileName ;
        }else{
            $upload = $file->storeAs($path, $fileName);
        }
       
        return $upload;
    }
}