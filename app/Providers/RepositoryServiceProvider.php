<?php

namespace App\Providers;

use App\Interfaces\{
    BookInterface, PageInterface, SubscriptionInterface,
    UserInterface
};

use App\Repositories\{
    BookRepository, PageRepository, SubscriptionRepository,
    UserRepository
};
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BookInterface::class, BookRepository::class);
        $this->app->bind(SubscriptionInterface::class, SubscriptionRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(PageInterface::class, PageRepository::class);
    }
}
