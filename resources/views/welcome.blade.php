@extends('layouts.guest')
@section('content')


    <section id="connected">
        <div class="row mt-0 mx-md-5 mx-3" id="connected-row">
            <div class="col-lg-6" style="margin: auto">
                <h2 class="font-weight-bold">
                    Stay connected from <br />from your home
                </h2>
                <p class="mt-3">
                    Get all the academic knowledge you ever need <br />By Subscribing
                    and reading our books online at <br />
                    a lower cost.
                </p>
            </div>
            <div class="col-lg-6 align-items-center">
                <img src="/assets/images/stacked.png" class="img-fluid" />
            </div>
        </div>
    </section>
    <section id="reading">
        <div class="row mt-0 mx-lg-5 mx-3" id="reading-row">
            <div class="col-lg-12 font-weight-bold mb-5">
                <h5 class="font-weight-bold">Start reading in 3 simple steps</h5>
            </div>

            <div class="col-lg-4">
                <div class="bg-white shadow-sm p-4">
                    <img src="/assets/images/sign-in.png" id="reading-icon" />
                    <h6 class="mt-4"><b>Sign in</b></h6>
                    <p class="mt-3">
                        Sign in to get access to all our books and get notifications
                        when we post and upload new books
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="bg-white shadow-sm p-4">
                    <img src="/assets/images/subscribe.png" id="reading-icon" />
                    <h6 class="mt-4"><b>Subscribe</b></h6>
                    <p class="mt-3">
                        Sign in to get access to all our books and get notifications
                        when we post and upload new books
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="bg-white shadow-sm p-4">
                    <img src="/assets/images/employee.svg" id="reading-icon" />
                    <h6 class="mt-4"><b>Read</b></h6>
                    <p class="mt-3">
                        Sign in to get access to all our books and get notifications
                        when we post and upload new books
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="scrollable-books">
        <div class="row mt-0 mx-lg-5 mx-3" id="scrollable-books-row">
            <div class="col-lg-12 font-weight-bold mb-2">
                <h5 class="font-weight-bold">Browse through books</h5>
            </div>

            @if ($books && $books->count() > 0)
                @foreach ($books as $book)
                    <x-book :book="$book" />
                @endforeach

            @else
                <div class="col-lg-3">
                    <div class="item p-3 pl-0">
                        <img src="/assets/images/books/2.jpg" class="img-fluid" />
                        <h6 class="font-weight-bold mt-2">Introductory technology...</h6>
                        <p>
                            <small><i>By Ezra, harry, ken and zuker</i></small>
                        </p>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="item p-3">
                        <img src="/assets/images/books/2.jpg" class="img-fluid" />
                        <h6 class="font-weight-bold mt-2">Introductory technology...</h6>
                        <p>
                            <small><i>By Ezra, harry, ken and zuker</i></small>
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="item p-3">
                        <img src="/assets/images/books/2.jpg" class="img-fluid" />
                        <h6 class="font-weight-bold mt-2">Introductory technology...</h6>
                        <p>
                            <small><i>By Ezra, harry, ken and zuker</i></small>
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="item p-3">
                        <img src="/assets/images/books/2.jpg" class="img-fluid" />
                        <h6 class="font-weight-bold mt-2">Introductory technology...</h6>
                        <p>
                            <small><i>By Ezra, harry, ken and zuker</i></small>
                        </p>
                    </div>
                </div>
            @endif

        </div>
    </section>

    <section id="download">
        <div class="row mt-0 mx-lg-5 mx-2" id="download-row">
            <div class="col-lg-6 align-items-center">
                <img src="/assets/images/collage.png" class="img-fluid" />
            </div>
            <div class="col-lg-6 align-items-center mt-4 mt-lg-auto" style="margin: auto; padding-left: 10%">
                <h2 class="font-weight-bold d-lg-block d-none">
                    Download books and <br />read offline
                </h2>

                <h3 class="font-weight-bold d-lg-none d-block">
                    Download books and <br />read offline
                </h3>
                <p class="mt-3">
                    Start reading all your education <br />books here
                </p>
            </div>
        </div>
    </section>

    <section id="faq">
        <div class="row mt-0 mx-md-5 mx-2" id="faq-row">
            <div class="col-lg-12">
                <h4 class="text-center font-weight-bold">
                    Frequently asked questions
                </h4>
            </div>

            <div class="col-md-8 offset-md-2">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne"
                            aria-expanded="true" aria-controls="collapseOne">
                            <p class="mb-0 font-weight-bold">
                                What is Studee?
                                <span class="float-right"><i class="fas fa-plus"></i></span>
                            </p>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life
                                accusamus terry richardson ad squid. 3 wolf moon officia
                                aute, non cupidatat skateboard dolor brunch.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo"
                            aria-expanded="false" aria-controls="collapseTwo">
                            <p class="mb-0 font-weight-bold">
                                What is Studee?
                                <span class="float-right"><i class="fas fa-plus"></i></span>
                            </p>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life
                                accusamus terry richardson ad squid. 3 wolf moon officia
                                aute, non cupidatat skateboard dolor brunch.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree"
                            aria-expanded="false" aria-controls="collapseThree">
                            <p class="mb-0 font-weight-bold">
                                What is Studee?
                                <span class="float-right"><i class="fas fa-plus"></i></span>
                            </p>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life
                                accusamus terry richardson ad squid. 3 wolf moon officia
                                aute, non cupidatat skateboard dolor brunch.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour"
                            aria-expanded="false" aria-controls="collapseThree">
                            <p class="mb-0 font-weight-bold">
                                What is Studee?
                                <span class="float-right"><i class="fas fa-plus"></i></span>
                            </p>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                            data-parent="#accordion">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life
                                accusamus terry richardson ad squid. 3 wolf moon officia
                                aute, non cupidatat skateboard dolor brunch.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @livewire('news-letter', ['instance' => 'home'])



@endsection
