<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="/vendors/styles/core.css">
    <link rel="stylesheet" type="text/css" href="/vendors/styles/icon-font.min.css">
   

    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400&family=Roboto&display=swap"
      rel="stylesheet"
    />
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <link rel="stylesheet" href="/assets/styles/dashboard.css" />

    <script src="/js/app.js"></script>

    @livewireStyles
    @livewireScripts

    <title>{{config('app.name')}} - @yield('title')</title>
</head>
@if (!Route::is('dashboard'))
<style>
  body {
        background-color: #f5f5f5;
      }
</style>
@endif
<body>
    
    <!-- Optional JavaScript; choose one of the two! -->

    {{-- @includeWhen(!auth()->user()->is_admin, '_inc.userHeader', ['status' => 'complete']) --}}

    @include('_inc.userHeader', ['status' => 'complete'])

    <x-message />
    @yield('content')
    
    @include('_inc.footer')

     <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="/vendors/scripts/core.js"></script>
    <script src="/vendors/scripts/script.min.js"></script>

</body>
</html>