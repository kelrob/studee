<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}} - Administrator Dashboard</title>

    <!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="/vendors/styles/style.css">
	@livewireStyles
	
</head>
<body>
    
	@include('_inc.admin-topbar')
	@include('_inc.admin-sidebar')
	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="row">
				<div class="col-md-10 mx-auto">
					@yield('content')

					<div class="footer-wrap pd-20 mb-20 card-box">
						{{config('app.name')}} Admin Dashboard
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="/vendors/scripts/core.js"></script>
	<script src="/vendors/scripts/script.min.js"></script>
	<script src="/vendors/scripts/process.js"></script>
	<script src="/vendors/scripts/layout-settings.js"></script>
	{{-- <script src="/assets/plugins/apexcharts/apexcharts.min.js"></script> --}}
	<script src="/assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="/vendors/scripts/dashboard.js"></script>

	<!-- js -->
	<!-- buttons for Export datatable -->
	<script src="/assets/plugins/datatables/js/dataTables.buttons.min.js"></script>
	<script src="/assets/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables/js/buttons.print.min.js"></script>
	<script src="/assets/plugins/datatables/js/buttons.html5.min.js"></script>
	<script src="/assets/plugins/datatables/js/buttons.flash.min.js"></script>
	<script src="/assets/plugins/datatables/js/pdfmake.min.js"></script>
	<script src="/assets/plugins/datatables/js/vfs_fonts.js"></script>
	<!-- Datatable Setting js -->
	<script src="/vendors/scripts/datatable-setting.js"></script>

	@livewireScripts
</body>
</html>