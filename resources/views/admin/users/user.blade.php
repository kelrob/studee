@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
        <div class="pd-20 card-box height-100-p">
            <div class="profile-photo">
                
                <img style="height: 100%;" src="{{auth()->user()->profile_photo_path == null ? 
                auth()->user()->getProfilePhotoUrlAttribute() : 
                Storage::url(auth()->user()->profile_photo_path) }}" alt="" class="avatar-photo">
                
            </div>
            <h5 class="text-center h5 mb-0">{{$user->username}}</h5>
            <p class="text-center text-muted font-14">{{ucfirst($user->firstname . ' ' . $user->lastname)}}</p>
            <div class="profile-info">
                <h5 class="mb-20 h5 text-blue">User Information</h5>
                <ul>

                     <li>
                        <span>Gender:</span>
                        {{ $user->is_male ?'Male' : 'Female'}}
                    </li>

                    <li>
                        <span>Email Address:</span>
                        {{$user->email}}
                    </li>
                    <li>
                        <span>Phone Number:</span>
                        {{$user->phone}}
                    </li>
                    <li>
                        <span>Joined Date:</span>
                        {{$user->created_at->format('d-m-Y')}}
                    </li>
                    <li>
                        <span>About:</span>
                        {{$user->about}}
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
        <div class="card-box height-100-p overflow-hidden">
            <div class="profile-tab height-100-p">
                <div class="pd-20">
                    <h4 class="h5 text-center">Library</h4>
                </div>

                <div class="mt-5">
                    <div class=" table-responsive">
                        <table class="data-table table nowrap">
                            <thead>
                                <tr>
                                    <th class="table-plus datatable-nosort">Cover</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($library as $book)
                                    <tr>
                                        <td class="table-plus">
                                            <img src="{{$book->book->cover}}" width="70" height="70" alt="">
                                        </td>
                                        <td>
                                            {{ucfirst($book->book->title)}}
                                        </td>
                                        <td>{{ucfirst($book->book->author)}}</td>
                                        <td>{{number_format($book->book->downloads->count())}}</td>
                                        
                                    </tr>
                                    
                                    {{-- @break($book->number == 10) --}}
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection