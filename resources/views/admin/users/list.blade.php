@extends('layouts.admin')
@section('content')
    <!-- Simple Datatable start -->
		<div class="card-box mb-30">
			<div class="pd-20">
				<h4 class="h5">{{ucfirst($title)}}</h4>
				<p class="mb-0"> total of {{count($users)}} user(s)</p>
			</div>
			<div class="pb-20">
				<table class="data-table table stripe hover nowrap">
					<thead>
						<tr>
							<th class="table-plus datatable-nosort">Name</th>
							<th>Username</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Start Date</th>
							<th class="datatable-nosort">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($users as $user)
							<tr>
								<td class="table-plus">{{ucfirst($user->firstname . ' ' . $user->lastname)}}</td>
								<td>{{$user->username}}</td>
								<td>{{$user->email}}</td>
								<td>{{$user->phone}}</td>
								<td>{{$user->created_at->format('d-m-Y')}}</td>
								<td>
									<div class="dropdown">
										<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
											<i class="dw dw-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
											<a class="dropdown-item" href="{{route('admin.user', encrypt($user->id))}}"><i class="dw dw-eye"></i> View Profile</a>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- Simple Datatable End -->
@endsection