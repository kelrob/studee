@extends('layouts.admin')
@section('content')
    <div class="card-box mb-30 pb-4">
        <h4 class="h5 pd-20">Recently Downloaded Books</h4>
    <div class=" table-responsive">
            <table class="data-table table nowrap">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Cover</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Downloads</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($books as $book)
                        <tr>
                            <td class="table-plus">
                                <img src="{{$book->cover}}" width="70" height="70" alt="">
                            </td>
                            <td>
                                {{ucfirst($book->title)}}
                            </td>
                            <td>{{ucfirst($book->author)}}</td>
                            <td>{{number_format($book->downloads->count())}}</td>
                            <td>
                                <div class="dropdown">
									<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
										<i class="dw dw-more"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
										<a class="dropdown-item" href="{{route('admin.book.edit', encrypt($book->id))}}"><i class="dw dw-edit2"></i> Edit</a>
									</div>
								</div>
                            </td>
                            
                        </tr>
                        
                        {{-- @break($book->number == 10) --}}
                    @endforeach
                </tbody>
            </table>
    </div>
    </div>
@endsection