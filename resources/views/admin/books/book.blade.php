@extends('layouts.admin')
@section('content')
    <div class="card-box mb-30 pb-4">
        <div class="pd-20">
            <h4 class="h5">Edit Book</h4>
            <p class="mb-0"> {{ucfirst($book->title)}}</p>
        </div>

        @livewire('edit-book', ['book' => $book])
    </div>


@endsection