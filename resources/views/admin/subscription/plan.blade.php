@extends('layouts.admin')
@section('content')
    <div class="card-box mb-30">
        <div class="pd-20">
            <h4 class="h5">Edit {{ucfirst($plan->name)}} Plans</h4>
        </div>
        <div class="mt-4 pd-20">
            <form action="{{route('admin.plan.update')}}" method="POST">
                @csrf
                <input type="hidden" name="plan" value="{{$plan->id}}" id="">
                <div class="mb-4">
                    <label for="">Plan Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Plan Name" value="{{$plan->name}}">
                    @error('name')
                        <span class="font-16 text-danger">{{$message}}</span>         
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="">Cost</label>
                    <input type="text" name="cost" class="form-control" placeholder="Plan Cost" value="{{$plan->cost}}">
                    @error('cost')
                        <span class="font-16 text-danger">{{$message}}</span>         
                    @enderror
                </div>

                <div class="row">
                    <div class="mb-4 col-md-4">
                        <label for="">Weekly Reads</label>
                        <input type="number" name="reads" class="form-control" placeholder="Weekly Reads" value="{{$plan->weekly_reads}}">
                        @error('reads')
                            <span class="font-16 text-danger">{{$message}}</span>         
                        @enderror
                    </div>

                    <div class="mb-4 col-md-4">
                        <label for="">Downloads</label>
                        <input type="number" name="downloads" class="form-control" placeholder="Downloads" value="{{$plan->downloads}}">
                        @error('downloads')
                            <span class="font-16 text-danger">{{$message}}</span>         
                        @enderror
                    </div>


                    <div class="mb-4 col-md-4">
                        <label for="">Notification</label>
                        <select name="downloads" class="form-control" placeholder="Downloads">
                            <option value="0"  {{ !$plan->books_notification ? 'selected' : ''}}>False</option>
                            <option value="1" {{ $plan->books_notification ? 'selected' : ''}}>True</option>
                        </select>
                        @error('downloads')
                            <span class="font-16 text-danger">{{$message}}</span>         
                        @enderror
                    </div>
                </div>

                <div class="mt-4 mb-5">
                    <button type="submit" class="btn btn-success">Save Plan</button>
                </div>
            </form>
        </div>
    </div>
@endsection