@extends('layouts.admin')
@section('content')
    <div class="card-box mb-30">
			<div class="pd-20">
				<h4 class="h5">Subscription Plans</h4>
			</div>
			<div class="pb-20">
				<table class="data-table table stripe hover nowrap">
					<thead>
						<tr>
							<th class="table-plus datatable-nosort">Plan Name</th>
							<th>Cost</th>
							<th>Weekly Reads</th>
							<th>Downloads</th>
							<th>Notification</th>
							<th class="datatable-nosort">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($plans as $plan)
							<tr>
								<td class="table-plus">{{ucfirst($plan->name)}}</td>
								<td>NGN{{number_format($plan->cost, 2)}}</td>
								<td>{{$plan->weekly_reads}}</td>
								<td>{{$plan->downloads}}</td>
								<td>{{$plan->book_notification ? 'True' : 'False'}}</td>
								<td>
									<div class="dropdown">
										<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
											<i class="dw dw-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
											<a class="dropdown-item" href="{{route('admin.plan.edit', encrypt($plan->id))}}"><i class="dw dw-eye"></i> Edit Plan</a>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
@endsection