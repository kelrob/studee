@extends('layouts.admin')
@section('content')
    <div class="card-box mb-20">
        <div class="pd-20 d-flex justify-content-between mb-5">
            <h4 class="h5">Genres</h4>
            <a href="#" class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#Medium-modal" >Add Genre</a>
        </div>
        <div class="pb-20">
            <div class=" table-responsive">
                <table class="data-table table nowrap">
                    <thead>
                        <tr>
                            <th class="table-plus datatable-nosort">Cover</th>
                            <th>Name</th>
                            <th>Books</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($genres as $genre)
                            <tr>
                                <td class="table-plus">
                                    <img src="{{$genre->image}}" width="70" height="70" alt="">
                                </td>
                                <td>
                                    {{ucfirst($genre->name)}}
                                </td>
                                <td>{{ucfirst($genre->books->count())}}</td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="{{route('admin.genre.edit', encrypt($genre->id))}}"><i class="dw dw-edit2"></i> Edit</a>
                                            <a class="dropdown-item" href="{{route('admin.genre.delete', encrypt($genre->id))}}"><i class="dw dw-delete-2"></i> Delete</a>
                                        </div>
                                    </div>
                                </td>

                            </tr>
                            
                            {{-- @break($download->number == 10) --}}
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    

    <div class="modal fade" id="Medium-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                
                <div class="modal-body">
                    <div class="d-flex justify-content-between mb-5 align-items-center">
                         <h4 class="h5" >Add Genre</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    @livewire('add-genre')
                </div>
            </div>
        </div>
    </div>
@endsection