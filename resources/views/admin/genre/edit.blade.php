@extends('layouts.admin')

@section('content')
    
    <div class="card-box mb-20">
        <div class="pd-20 d-flex justify-content-between mb-5">
            <h4 class="h5">Edit Genre</h4>
            
        </div>
        <div class="pb-20 py-5">
            <div class="px-5">
                <form action="{{route('admin.genre.update')}}" enctype="multipart/form-data" method="POST">
                @csrf
                <input type="hidden" name="genre" value="{{$genre->id}}">
                 <div class="mt-5">
                    <div class="mb-4">
                        <label for=""> Genre Name</label>
                        <input type="text" value="{{$genre->name}}" name="name" class="form-control" placeholder="Name">
                        @error('name')
                            <span class="text-danger text-14 mt-1">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="mb-4">
                        <label for="">Image</label>
                        
                        <div>
                            <input type="file"  name="image" accept="image/*" >
                        </div>
                        @error('image')
                            <span class="text-danger text-14 mt-1">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary float-right">Save</button>
            </form>
            </div>
        </div>
    </div>
@endsection