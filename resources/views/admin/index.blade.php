@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-4 mb-30">
        <div class="card-box height-100-p widget-style1">
            <div class="d-flex flex-wrap align-items-center">
                <div class="widget-data py-3">
                    <div class="weight-600 font-14">Users</div>
                    <div class="h4 mb-0">{{number_format($users->count())}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 mb-30">
        <div class="card-box height-100-p widget-style1">
            <div class="d-flex flex-wrap align-items-center">
                
                <div class="widget-data py-3">
                    <div class="weight-600 font-14">Books</div>
                    <div class="h4 mb-0">{{number_format($books->count())}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 mb-30">
        <div class="card-box height-100-p widget-style1">
            <div class="d-flex flex-wrap align-items-center">
                
                <div class="widget-data py-3">
                    <div class="weight-600 font-14">Downloads</div>
                    <div class="h4 mb-0">{{number_format($downloads->count())}}</div>
                </div>
            </div>
        </div>
    </div>
    
</div>

<div class="card-box mb-30 pb-4">
    <h4 class="h5 pd-20">Recently Added Books</h4>
   <div class=" table-responsive">
        <table class="data-table table nowrap">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Cover</th>
                    <th>Title</th>
                    <th>Author</th>
                    {{-- <th>Downloads</th> --}}
                    {{-- <th>Actions</th> --}}
                </tr>
            </thead>
            <tbody>

                @foreach ($books->take(8) as $book)

                    
                    <tr>
                        <td class="table-plus">
                            <img src="{{$book->cover}}" width="70" height="70" alt="">
                        </td>
                        <td>
                            {{ucfirst($book->title)}}
                        </td>
                        <td>{{ucfirst($book->author)}}</td>
                        {{-- <td>{{number_format($download->book->downloads->count())}}</td> --}}
                        
                    </tr>
                    
                    @break($book->number == 10)
                @endforeach
            </tbody>
        </table>
   </div>
</div>
    
@endsection