<div>
   <form action="{{route('admin.genre')}}" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="mt-5">
            <div class="mb-4">
                <label for=""> Genre Name</label>
                <input type="text" name="name" class="form-control" placeholder="Name">
                @error('name')
                    <span class="text-danger text-14 mt-1">{{$message}}</span>
                @enderror
            </div>

            <div class="mb-4">
                <label for="">Image</label>
                
                <div>
                    <input type="file" name="image" accept="image/*" >
                </div>
                @error('image')
                    <span class="text-danger text-14 mt-1">{{$message}}</span>
                @enderror
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Save</button>
   
    </form>
</div>
