<div class="pd-20 pb-5">
    <form action="{{route('admin.book.save')}}" enctype="multipart/form-data" method="POST">
        @csrf

        <h4 class="h4 mb-2"> Book Details </h4>
        <div class="mb-4">
            <label for=""> Title</label>
            <input type="text" name="title" value="{{old('title')}}" class="form-control" placeholder="Book Title">
            @error('title')
                <span class="text-danger text-14 mt-1">{{$message}}</span>
            @enderror
        </div>
        <div class="mb-4">
            <label for=""> Author Name</label>
            <input type="text" name="author" value="{{old('author')}}" class="form-control" placeholder="Book Author(s)">
            @error('author')
                <span class="text-danger text-14 mt-1">{{$message}}</span>
            @enderror
        </div>

        <div class="row">
            <div class="mb-4 col-md-6">
                <label for="">Genre</label>
                <select  name="genre" class="form-control">
                    <option value="">Select genre</option>
                    @foreach ($genres as $genre)
                        <option value="{{$genre->id}}">{{ucfirst($genre->name)}}</option>
                    @endforeach
                </select>
                @error('genre')
                    <span class="text-danger text-14 mt-1">{{$message}}</span>
                @enderror
            </div>
            <div class="mb-4 col-md-6">
                <label for=""> Publish Date</label>
                <input type="date" name="publish_date" class="form-control" value="{{old('publish_date')}}" placeholder="DD-MM-YYYY">
                @error('publish_date')
                    <span class="text-danger text-14 mt-1">{{$message}}</span>
                @enderror
            </div>
        </div>
        
        <div class="mb-4">
            <label for=""> About Book</label>
            <textarea name="description" class="form-control" value="{{old('description')}}" placeholder="Book Description"></textarea>
            @error('description')
                <span class="text-danger text-14 mt-1">{{$message}}</span>
            @enderror
        </div>

        <div class="row">
            <div class="mb-4 col-md-6">
                <label for="">Book Cover</label>
                <div class="custom-file mb-2">
                    <input type="file" name="cover" id="cover" accept="image/*" class=" file-upload">
                    {{-- <label class="custom-file-label" for="cover">Choose Cover Image</label> --}}
                </div>
                @error('cover')
                    <span class="text-danger text-14 mt-2">{{$message}}</span>
                @enderror
            </div>

            <div class="mb-4 col-md-6">
                <label for="">Book File (PDF)</label>
                <div class="custom-file mb-2">
                    <input type="file" name="file" id="file" accept=".pdf" class="file-upload">
                    {{-- <label class="custom-file-label" for="file">Choose file File</label> --}}
                </div>
                @error('file')
                    <span class="text-danger text-14 mt-2">{{$message}}</span>
                @enderror
            </div>
        </div>

        {{-- <div class="mt-4 mb-4 pd-20">
            <div class="d-flex justify-content-between align-items-center">
                <h4 class="h4 mb-2"> Book Pages </h4>
                <a href="#" wire:click.prevent="add({{$i}})" class="btn-sm btn-primary">Add Page</a>
            </div>
            <div class="row">
                <div class="mb-4 col-md-6 mt-4">
                    <label for="">Page Title</label>
                    <input type="text" name="page_title[0]" class="form-control" placeholder="Page Title">
                    @error('page_title[0]')
                        <span class="text-danger text-14 mt-1">{{$message}}</span>
                    @enderror
                </div>
                <div class="mb-4 col-md-6 mt-4">
                    <label for="">Page File (PDF)</label>
                    <div class="custom-file">
                        <input type="file" id="page_file[0]" name="page_file[0]" class="custom-file-input">
                        <label class="custom-file-label" for="page_file[0]">Choose Page Image</label>
                    </div>
                    @error('page_file[0]')
                        <span class="text-danger text-14 mt-1">{{$message}}</span>
                    @enderror
                </div>
            </div>

            @foreach ($inputs as $key => $value)
                <div class="row position-relative">
                    <a href="" wire:click.prevent="remove({{$key}})" style="right: 0; margin-right: 10px;" class=" position-absolute btn rounded-circle btn-danger px-2 py-0 font-20 text-center inline-block">&times;</a>
                    <div class="mb-4 col-md-6 mt-4">
                        <label for="">Page Title</label>
                        <input type="text" name="page_title[{{$value}}]" class="form-control" placeholder="Page Title">
                        @error('page_title['.$value. ']')
                            <span class="text-danger text-14 mt-1">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="mb-4 col-md-6 mt-4">
                        <label for="">Page File (PDF)</label>
                        <div class="custom-file">
                            <input type="file" id="page_file[{{$value}}]" name="page_file[{{$value}}]" class="custom-file-input">
                            <label class="custom-file-label" for="page_file[{{$value}}]">Choose Page Image</label>
                        </div>
                        @error('page_file['.$value . ']')
                            <span class="text-danger text-14 mt-1">{{$message}}</span>
                        @enderror
                    </div>
                </div>
            @endforeach
        </div> --}}
        <div class="mb-4">
            <button type="submit" class="btn btn-success float-right">Save Book</button>
        </div>
    </form>
</div>
