<div x-data="{ openNavigator: false }" class="px-3 position-absolute row d-xl-none d-block pb-5"
    x-bind:class="! openNavigator ? 'bg-transparent' : 'bg-white'" style="z-index: 10; top: 12%;">
    <button class="btn btn-outline-secondary navigator" @keyup.escape="openNavigator = false"
        @click.outside="openNavigator = false" @click.prevent="openNavigator = true"> Nav </button>
    <div x-bind:class="! openNavigator ? 'd-none animate__fadeOut' : 'animate__animated animate__fadeIn'">
        <div class="row mt-3 mt-md-1 pb-5 mb-4 px-3" x-show="openNavigator">
            <div class="col-md-8 offset-md-2 col-xl-3 p-xl-5">
                <div class="">
                    <div class="mb-4 w-100" style="z-index: -1000;">
                        <img src="{{ $book->cover }}" alt="" class=" img-fluid">
                    </div>

                    <div class="mb-4 card w-100">
                        <div class="card-body py-3">
                            <h6 class="text-center">Table of Content</h6>
                            <div class="mt-3">
                                @foreach ($book->pages as $page)
                                    <a href="" wire:click.prevent="selectPage({{ $page }})"
                                        class="mb-3">
                                        <h6><small><b>{{ truncateString($page->title, 20, '___') }}</b></small></h6>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row -mt-4" style="margin-top: -4%;margin-bottom: -8%;">
    <div class="col-md-5 col-xl-3 bg-white p-xl-5 d-none d-xl-block">
        <div class="">
            <div class="mb-4 w-100">
                <img src="{{ $book->cover }}" alt="" class=" img-fluid">
            </div>

            <div class="mb-4 card w-100">
                <div class="card-body py-3">
                    <h6 class="text-center">Table of Content</h6>
                    <div class="mt-3">
                        @foreach ($pages as $page)
                            <a href="" wire:click.prevent="selectPage({{ $page }})" class="mb-3">
                                <h6><small><b>{{ truncateString($page->title, 20, '___') }}</b></small></h6>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-10 col-xl-9 py-md-5">
        <div class="row">
            <div class="col-xl-8 offset-md-2 ">
                <div class="card py-3">
                    <div class="card-body">
                        <div class="text-center mb-4">
                            <h4>Introduction</h4>
                        </div>

                        {{-- <script src="https://unpkg.com/pdfjs-dist@2.0.489/build/pdf.min.js"></script>
                        <script src={{ url('/assets/index.js') }}></script>
                        <script>
                            initPDFViewer({{ asset('storage/' . $book->downloadable->path) }});
                        </script> --}}

                        <iframe src="{{ asset('storage/' . $book->downloadable->path) }}"
                            style="width:100%; height: 800px; border: none; overflow-y: scroll;"></iframe>

                        {{-- <div class="py-2 px-xl-5 px-2 text-justify">
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                Illum rerum quo dolores. Sunt inventore magni veniam perspiciatis similique
                                atque architecto voluptatibus iusto unde error id, dolore repellat possimus
                                est minus ullam, voluptatem obcaecati quas dolores dicta ab fugiat! Id tempore
                                inventore ducimus eaque mollitia aliquid unde vel possimus corrupti, expedita enim
                                sunt laborum quae odio molestiae quasi, fugit harum. Vel alias eaque animi omnis
                                voluptatem quod adipisci! Consectetur laudantium commodi iure provident itaque aliquam
                                vitae dolorem doloremque dignissimos, cum ex blanditiis eaque maxime animi obcaecati
                                esse fuga quaerat nulla velit iste sequi non temporibus. Quam pariatur molestiae quis
                                nihil iusto. Odio aspernatur earum ex omnis repellat architecto eligendi optio quis
                                ullam? Ab et neque dignissimos assumenda praesentium eum explicabo repellat nam sint,
                                aliquid natus vel saepe ipsam asperiores quaerat unde modi optio omnis iste quod sunt
                                exercitationem distinctio eveniet. Quis labore vero inventore tenetur sed laboriosam, dolore in
                                libero totam pariatur? Veritatis voluptatibus quisquam eos accusamus ipsam quia aliquam. Similique saepe tempore
                                veritatis architecto neque harum, necessitatibus, aut repudiandae corporis magnam perferendis
                                in a. Nobis minus perferendis distinctio totam dolor atque harum itaque deserunt rerum consequuntur
                                maiores cupiditate, ipsam earum molestias
                                nam repellendus, aliquam nulla dolore rem! Harum, animi facilis!
                            </div> --}}
                    </div>

                    <div class="mt-5 mb-4 text-center">
                        <h5><small>Page 1</small></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
