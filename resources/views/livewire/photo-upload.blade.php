<div>
   
    <a href="#" onclick="event.preventDefault(); document.getElementById('file').click()">
        <img
            class="img-fluid w-100 " style="border-radius: 100%; height: 260px;"
            src="{{$image == null ? ( auth()->user()->profile_photo_path == null ? 
                auth()->user()->getProfilePhotoUrlAttribute() : 
                Storage::url(auth()->user()->profile_photo_path) ) : $image->temporaryUrl() }}"
        />
    </a>

    <form wire:submit.prevent="upload_photo" class="text-center mt-4">
        <input type="file" wire:model="image" class="d-none" name="photo" id="file">
        @if ($image != null)
            <button type="submit" class="btn btn-primary btn-sm">Update</button>
        @endif
    </form>
    
</div>
