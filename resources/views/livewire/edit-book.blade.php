<div class="pd-20 pb-5">
    <form action="{{route('admin.book.update')}}" method="POST" enctype="multipart/form-data">
        @csrf
        
        <input type="hidden" name="book" value="{{$book->id}}">
        <div class="mb-4">
            <label for=""> Title</label>
            <input type="text" name="title" class="form-control" placeholder="Book Title" value="{{$book->title}}">
            @error('title')
                <span class="text-danger text-14 mt-1">{{$message}}</span>
            @enderror
        </div>
        <div class="mb-4">
            <label for=""> Author Name</label>
            <input type="text" name="author" class="form-control" placeholder="Book Author(s)" value="{{$book->author}}">
            @error('author')
                <span class="text-danger text-14 mt-1">{{$message}}</span>
            @enderror
        </div>

        <div class="row">
            <div class="mb-4 col-md-6">
                <label for="">Genre</label>
                <select  name="genre" class="form-control">
                    <option value="">Select genre</option>
                    @foreach ($genres as $genre)
                        <option value="{{$genre->id}}" {{$genre->id == $book->genre_id ? 'selected' : ''}}>{{ucfirst($genre->name)}}</option>
                    @endforeach
                </select>
                @error('genre')
                    <span class="text-danger text-14 mt-1">{{$message}}</span>
                @enderror
            </div>
            <div class="mb-4 col-md-6">
                <label for=""> Publish Date</label>
                <input type="text" name="publish_date" class="form-control" placeholder="DD-MM-YYYY" value="{{now()->parse($book->published_at)->format('d-m-Y')}}">
                @error('publish_date')
                    <span class="text-danger text-14 mt-1">{{$message}}</span>
                @enderror
            </div>
        </div>

        <div class="mb-4">
            <label for=""> About Book</label>
            <textarea class="form-control" name="description" placeholder="Book Description">{{$book->about}}</textarea>
            @error('description')
                <span class="text-danger text-14 mt-1">{{$message}}</span>
            @enderror
        </div>

        <div class="mb-4">
            <label for="">Book Cover</label>
            <input type="file" name="cover" class="file-input">
            @error('cover')
                <span class="text-danger text-14 mt-1">{{$message}}</span>
            @enderror
        </div>
        <div class="mb-4">
            <button type="submit" class="btn btn-success float-right">Save Book</button>
        </div>
    </form>
</div>