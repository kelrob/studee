@if ($instance == 'footer')
    <div class="mt-4">
        <form action="" wire:submit.prevent="subscribe">
            <div class="form-group">
                <label for="email"
                ><small class="font-weight-bold"
                    >{{session('success') ? session('success') : 'Your email address here'}}</small
                ></label
                >
                <input type="email" wire:model="email" class="form-control" />
                @error('email')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group">
                <button
                    class="btn btn-primary border-0 btn-sm"
                    id="primary-bg"
                    type="submit"
                >
                    <small>SUBSCRIBE</small>
                </button>
            </div>
        </form>
    </div>   
@else
    <section id="subscribe">
        <div class="row mt-0 mx-2 mx-md-5" id="subscribe-row">
          <div class="col-lg-12">
            <p class="text-center">
                {{session('success') ? session('success') : 'Get all the academic knowledge you ever wished for on our site'}}
              
            </p>
          </div>

          <div class="col-lg-12" align="center">
            <form wire:submit.prevent="subscribe" action="" >
                <div id="search-form" class="d-none d-md-block">
                    <div class="input-group mb-3">
                      <input
                        type="text"
                        wire:model="email"
                        class="form-control pt-4 pb-4"
                        id="search-box"
                        placeholder="Enter Email address"
                      />
                      <div class="input-group-append">
                        <button
                          class="btn btn-primary border-0 pl-3 pr-3"
                          id="primary-bg"
                          type="submit"
                        >
                          Subscribe
                        </button>
                      </div>
                    </div>
                </div>

                <div style="width: 100%;">
                    <div class="form-group mb-3 d-xl-none d-block">
                        <input type="text" class="form-control" id="search-box"
                            placeholder="Enter Email address"/>
                        <div class="input-group-append">
                            <button class="btn btn-primary border-0 btn-block pl-3 pr-3" id="primary-bg"
                                type="submit">
                                Subscribe
                            </button>
                        </div>
                    </div>
                </div>

                @error('email')
                    <small class="">{{$message}}</small>
                @enderror
              
            </form>
          </div>
        </div>
      </section> 
@endif
