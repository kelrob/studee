 <div class="bg-white py-5">
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-3 side-bar d-none d-md-block">
                    <h5 class="font-weight-bold mb-4">GENRES</h5>

                    <div class="bg-light container py-2" style=" border-radius: 2px;">
                        <ul class="genres">
                            <li tabindex="1" onclick="document.getElementById('link').click();" class="mb-0">
                                <a href="{{route('books')}}" id="link">All Books</a>
                            </li>
                            @foreach ($genres as $genreSingle)
                                <li tabindex="1" onclick="document.getElementById('link-{{$genreSingle->id}}').click();" class="mb-0">
                                    <a href="{{route('books', $genreSingle->name)}}" 
                                            {{-- wire:click.prevent="selectGenre({{$genreSingle->id}})"  --}}
                                            id="link-{{$genreSingle->id}}">{{ucfirst($genreSingle->name)}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>


                <div class="col-md-9 py-5 px-3">
                  
                    <section class="top-picks mt-5 mb-5">
                        <div class="row mt-0 trending-row">
                            <div class="col-lg-12 mb-3">
                                <h5 class="font-weight-bold mb-4">
                                    @if ($genre == true)
                                        {{ucfirst($genre->name)}}
                                    @elseif(Route::is('books.search'))
                                        {{$books != null ? $books->count() . ' Results Found' : 0 . ' Results found' }}
                                    @else
                                        All Books
                                    @endif
                                </h5>
                            </div>
                        </div>

                        @if ($books != null && $books->count() > 0)
                            <div class="row" id="book-master">
                                @foreach ($books as $book)
                                    <x-book :book="$book"/>
                                @endforeach
                            </div>
                        @endif

                        @if ($books == null || $books->count() == 0)
                            <x-empty-card />
                        @endif
                    </section>

                   

                    @if ($topPicks != null && count($topPicks) > 0)
                        <section class="top-picks mb-5">
                            <div class="row mt-0 trending-row">
                                <div class="col-lg-12 mb-3">
                                    <h5 class="font-weight-bold">TOP PICKS FOR YOU</h5>
                                </div>
                            </div>
                            
                                <div class="row" id="book-master">

                                    @foreach ($topPicks as $book)
                                        <x-book :book="$book"/>
                                    @endforeach
                            
                            </div>
                        </section>
                    @endif


                    <div class="container">
                        <div class="divider"></div>
                    </div>

                    @if ($recentlyPublished != null && $recentlyPublished->count() > 0)

                        <section class="trending mb-5">
                                <div class="row mt-0 trending-row">
                                    <div class="col-lg-12" id="trending-heading">
                                        <h5 class="font-weight-bold">RECENTLY PUBLISHED</h5>
                                    </div>
                                </div>
                                
                                
                                    <div class="row mt-4" id="book-master">
                                        @foreach ($recentlyPublished as $book)
                                            <x-book :book="$book"/>
                                        @endforeach
                                    </div>
                            </section>
                    @endif

                    
                   </div>
                </div>
            </div>
        </div>
    </div>