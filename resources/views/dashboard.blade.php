@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
@if ($genres->count() > 0)
      <section class="subjects container">
        <div class="row">
          <div class="col-lg-12 mb-4">
            <h5 class="font-weight-bold">BROWSE GENRES</h5>
          </div>
        </div>
    
    
        <div class="row mt-4" id="book-master">
          @foreach ($genres as $genre)
            <x-genre :genre="$genre" />   
          @endforeach
        </div>

      </section>
@endif


@if (count($trendingBooks) > 0)
   <section class="trending">
     <div class="row mt-0 trending-row">
       <div class="col-lg-12" id="trending-heading">
         <h5 class="font-weight-bold">TRENDING BOOKS</h5>
        </div>
      </div>
      
          <div class="row mt-4" id="book-master">
            @foreach ($trendingBooks as $book)
                @if ($loop->first)
                    <x-book-detailed :book="$book"/>
                  @else
                  <x-book :book="$book"/>
                @endif 
            @endforeach
          </div>
    
    </section>
    <div class="container">
      <div class="divider"></div>
    </div>
  @endif


  @if (count($topPicks) > 0)

  <section class="top-picks">
    <div class="row mt-0 trending-row">
        <div class="col-lg-12 mb-3">
          <h5 class="font-weight-bold">TOP PICKS FOR YOU</h5>
        </div>
      </div>

          <div class="row" id="book-master">

            @foreach ($topPicks as $book)
                <x-book :book="$book"/>
            @endforeach
          
          </div>

  </section>

@endif
    

    @if (auth()->user()->is_subscribed())
      <section id="plan">
        <div class="row mt-0 ml-5 mr-5" id="plan-row">
          <div class="col-lg-12">
            <h3 class="text-center text-white font-weight-normal">
              Upgrade your plan and get over 1000 books
            </h3>
          </div>
        </div>
      </section>
      <div class="container mt-5">
      <div class="divider"></div>
    </div>
    @endif

    {{-- <section class="subjects container">
      <div class="row mb-lg-5 mb-2 mt-3 mt-lg-3">
        <div class="col-lg-12">
          <h5 class="font-weight-bold">EDITORS CHOICE</h5>
        </div>

        <div class="col-lg-3 col-6 mt-3 mt-lg-0">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-3">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-3 mt-lg-0">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-3">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-3 mt-lg-0">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-3">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-3 mt-lg-0">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-3">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-3 mt-lg-0">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-3">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
      </div>
    </section> --}}

    

    {{-- <section class="subjects container">

      <div class="row mb-lg-5 mb-2 mt-3 mt-lg-3">
        <div class="col-lg-12">
          <h5 class="font-weight-bold">POPULAR CLASSIC</h5>
        </div>

        <div class="col-lg-3 col-6 mt-4">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-2">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-4">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-2">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-4">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-2">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-4">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-2">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-6 mt-4">
          <div class="item pl-0">
            <img src="./assets/images/books/2.jpg" class="img-fluid" />
            <div class="mt-2">
              <span class="tag"
                ><small>by</small>
                <small class="font-weight-bold">Longman Rich</small></span
              >
            </div>
          </div>
        </div>
      </div>
      
    </section> --}}

@if ($recentlyPublished->count() > 0)
    <section class="trending">
      <div class="row mt-0 trending-row">
        <div class="col-lg-12" id="trending-heading">
          <h5 class="font-weight-bold">RECENTLY PUBLISHED</h5>
        </div>
      </div>

        <div class="row mt-4" id="book-master">
          @foreach ($recentlyPublished as $book)
              @if ($loop->first)
                  <x-book-detailed :book="$book"/>
                @else
                <x-book :book="$book"/>
              @endif 
          @endforeach
        </div>        
@endif
@endsection