@extends('layouts.app')
@section('title', 'Logout')
@section('content')
    <div class="container-fluid">
      <div class="row mt-4">
        <div class="col-md-4 offset-md-4 text-center bg-white rounded p-4">
          <div class="mt-5">
            <h4>
              Oh no, you're leaving... <br />
              Are you sure?
            </h4>

            <p class="mt-4">
              <a
              href="{{url()->previous()}}"
                class="btn btn-primary border-0 logout-btn"
                id="btn-primary"
              >
                Nah!! Just kidding
              </a>
            </p>

            <p class="mt-4">
              <form action="{{route('logout')}}" method="POST">
                @csrf
                  <button class="btn btn-default border-secondary logout-btn">
                    Yes, Log me out
                  </button>
              </form>
            </p>
          </div>
        </div>
      </div>
    </div>
@endsection