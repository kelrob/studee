@extends('layouts.auth')
@section('title', 'Register')
@section('content')
<div class="row m-0 p-0">
    <div class="col-lg-6 p-3 d-none d-md-block" id="login-content" style="margin: 0 auto">
        <div class="child">
        <h2 class="font-weight-normal">
            Continue your <br />Reading journey with us
        </h2>
        <p class="font-weight-light mt-4" style="font-weight: 100">
            We are always here to serve you better
        </p>
        </div>
    </div>
    <div class="col-lg-6" id="login-form">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div align="center">
                <img src="/assets/images/logo-darks.png" />

                <h5 class="mt-4 font-weight-bold">
                    <small>Create an Account</small>
                </h5>
                </div>
                <form method="POST" action="{{route('register')}}" id="register-form">
                    @csrf
                    <div class="form-group">
                        <input
                            type="text"
                            name="name"
                            placeholder="User name"
                            value="{{old('name')}}" 
                            autofocus 
                            autocomplete="name"
                            class="form-control"
                        />

                        @error('name')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input
                            type="text"
                            name="email"
                            placeholder="Email"
                            class="form-control"
                            value="{{old('email')}}"
                        />
                        @error('email')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input
                            type="text"
                            name="phone"
                            placeholder="Phone No"
                            class="form-control"
                            value="{{old('phone')}}"
                        />
                        @error('phone')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        autocomplete="new-password"
                        class="form-control"
                        />
                        @error('password')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input
                            type="password"
                            placeholder="Confirm Password"
                            name="password_confirmation" 
                            autocomplete="new-password"
                            class="form-control"
                        />
                    </div>
                    <div id="remember" class="text-center mt-2">
                        <small class=" text-primary">By Signing up you agree to our terms and conditions</small>
                    </div>
                    <div class="form-group text-center">
                        <button
                            type="submit"
                            class="btn btn-primary border-0 btn-sm login-btn"
                            id="primary-bg"
                        >
                            REGISTER
                        </button>
                    </div>
                </form>

                <div id="signup-text">
                <p class="text-center">
                    <small>Already have an account? <a href="{{route('login')}}"><b>Login</b></a></small></p>
                </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection