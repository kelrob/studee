@extends('layouts.app')
@section('title', 'Verify Email')
@section('content')
    <div class="container-fluid">
        <div class="row mt-4">
            <div class="col-md-4 offset-md-4 text-center bg-white rounded p-4">
                <div class="mt-5">
                    <h4 class="mb-4">Email Verification</h4>
                    <p style="line-height: 35px;">
                        Thanks for signing up! Before getting started,
                        could you verify your email address by clicking on the link we just emailed to you? If you didn't
                        receive the email,
                        we will gladly send you another.
                    </p>

                    @if (session('status') == 'verification-link-sent')
                        <div class="mb- alert alert-success">
                            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('verification.send') }}">
                        @csrf

                        <div>
                            <button type="submit" class="btn btn-info border-0 logout-btn">
                                {{ __('Resend Link') }}
                            </button>
                        </div>
                    </form>

                    <p class="mt-2">
                        <a href="{{ route('logout') }}" class="btn btn-danger logout-btn">
                            Yes, Log me out
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
