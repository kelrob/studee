
@extends('layouts.auth')
@section('title', 'Login')
@section('content')
    <div class="row m-0 p-0">
        <div class="col-lg-6 p-3 d-none d-md-block" id="login-content" style="margin: 0 auto">
          <div class="child">
            <h2 class="font-weight-normal">
              Continue your <br />Reading journey with us
            </h2>
            <p class="font-weight-light mt-4" style="font-weight: 100">
              We are always here to serve you better
            </p>
          </div>
        </div>
        <div class="col-lg-6" id="login-form">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div align="center">
                    <img src="./assets/images/logo-darks.png" />

                    <h5 class="mt-4 font-weight-bold">
                        <small>Login to get Started</small>
                    </h5>
                    </div>
                    <form method="POST" action="{{route('login')}}" id="login-form">
                        @csrf
                        <div class="form-group">
                            <input
                            type="text"
                            name="email"
                            placeholder="Email"
                            class="form-control"
                            value="{{old('email')}}" autofocus
                            />
                            @error('email')
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input
                            type="password"
                            name="password"
                            placeholder="Password"
                            autocomplete="current-password" 
                            class="form-control"
                            />
                            @error('password')
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div id="remember">
                            <input type="checkbox" id="remember_me" name="remember" /> <small>Remember me</small>
                            @if (Route::has('password.request'))
                            <span class="float-right" >
                                <a href="{{ route('password.request') }}"><small>Forgot Password</small></a>
                            </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button
                                type="submit"
                                class="btn btn-primary border-0 btn-sm login-btn"
                                id="primary-bg"
                                >
                                LOGIN
                            </button>
                        </div>
                    </form>

                    <div id="signup-text" class="mt-5">
                    <p class="text-center">
                        <small>Don't have an account? <a href="{{route('register')}}"><b>Signup</b></a></small></p>
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection