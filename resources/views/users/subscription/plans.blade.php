@extends('layouts.app')
@section('title', 'Subscription Plans')
@section('content')
    <div class="container-fluid my-5">
      <div class="row mt-4">
        <div class="col-lg-12 text-center">
          <h4 class="font-weight-bold">
            <span id="primary-color">Flexible</span> Plans
          </h4>
          <p style="font-size: 15px" class="mt-3">
            Choose a plan that works best for you <br />
            and your team
          </p>
        </div>
      </div>

      <div class="container mb-5">
        <div class="row mt-4">
          @foreach ($plans as $plan)
         
          <div class="col-md-4 mt-4 position-relative">
              <form action="{{route('subscribe')}}" method="POST">
                 @csrf
                  <input type="hidden" name="plan" value="{{$plan->id}}">
                  <div class="shadow py-5 px-4 bg-white" style="border-radius: 12px;">
                    @if ($plan->name == 'Starter')
                        <span class=" badge badge-danger popular-badge shadow-sm">
                          popular
                        </span>
                    @endif
                    <div class=" px-3">
                      <div class="row align-content-center align-items-center">
                          <div class="col-3 pr-0">
                              <img
                                  src="/assets/images/sign-in.png"
                                  alt=""
                                  style="width: 100%;"
                                  class="img-fluid"
                              />
                          </div>
                          <div class="col-9 px-3">
                              <h6 class="m-0 font-weight-bold">{{$plan->name}}</h6>
                              <h6 class="mt-1">
                                  <span style="font-weight: 900;">NGN{{number_format($plan->cost)}}</span
                                  ><span style="font-size: 14px">/month</span>
                              </h6>
                          </div>
                      </div>
                      <hr style=" color: #959595;">
                    </div>
                    <div class="px-3">
                        <div class="mt-0 d-flex  align-items-center">
                            <span class=" mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" style="height: 1.4rem;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                </svg>
                            </span>
                            <small>Read <span style="font-weight: 900;">{{$plan->weekly_reads == 0 ? 'unlimited' :'a max of '. $plan->weekly_reads}} books</span> {{ $plan->weekly_reads > 0 ?'a week': ''}}</small>
                        </div>
                        <div class="mt-3 d-flex align-items-center ">
                            <span class=" mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" style="height: 1.4rem;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                </svg>
                            </span>
                            <small>Download  <span style="font-weight: 900;">{{$plan->downloads == 0 ? 'unlimited' :'a max of ' . $plan->downloads}} books</span> {{ $plan->download > 0 ?'a week': ''}}</small>
                        </div>
                        <div class="mt-3 d-flex align-items-center ">
                            <span class=" mr-2">
                                @if (!$plan->books_notification)
                                    <svg xmlns="http://www.w3.org/2000/svg"  style="height: 1.4rem;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                @else
                                    <svg xmlns="http://www.w3.org/2000/svg" style="height: 1.4rem;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                    </svg>
                                @endif
                            </span>
                            <small>
                              @if (!$plan->books_notification)
                                  Not notified of <span style="font-weight: 900;">new books</span>
                              @else
                                  Get notified of <span style="font-weight: 900;">new books</span>
                              @endif
                            </small>
                        </div>
                    </div>
                    <div class="mt-4 px-3">
                      <button type="submit" class="btn btn-block btn-danger">Choose Plan <span class="font-weight-light">&rightarrow;</span></button>
                    </div>
                  </div>
              </form>
          </div>
          @endforeach
        </div>
      </div>
    </div>
@endsection