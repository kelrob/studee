@extends('layouts.app')
@section('title', 'Change Password')
@section('content')


<div class="bg-white py-5 pass-wrap"">

    <div class="mt-5 container">
        <div class="row">
            <div class="col-lg-4 d-none d-lg-block align-items-end">
                <img src="/assets/images/forgot-password.svg" style="height: 18rem; width: 100%;" alt="">
            </div>
            <div class="col-md-8 col-lg-5 offset-md-2 px-lg-5">
                <div class="mb-4">
                    <h4>Change Password</h4>
                </div>
                <form method="POST" action="{{route('profile.password.update')}}">
                    @csrf
                    <div class="form-group">
                        <label for="">New Password</label>
                        <input
                        type="password"
                        name="password"
                        placeholder="********"
                        class="form-control"
                        value="{{old('password')}}" autofocus
                        />
                        @error('password')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Confirm New Password</label>
                        <input
                        type="password"
                        name="password_confirmation"
                        placeholder="********"
                        autocomplete="new-password" 
                        class="form-control"
                        />
                        @error('password')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    
                    <div class="form-group mt-4 d-flex justify-content-between">
                        <a
                            href="{{url()->previous()}}"
                            style="width: 48%;"
                            class="btn btn-outline-secondary"
                            >
                            Cancel
                        </a>

                        <button
                            type="submit"
                            style="width: 48%;"
                            type="submit"
                            class="btn btn-danger"
                            >
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
    
@endsection