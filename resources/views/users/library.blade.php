@extends('layouts.app')
@section('title', 'My Library')

@section('content')
    <div class="container">
        <div class="row mb-5">
          <div class="col-lg-6">
            <h1 class="hero-header text-dark">
              Build your Library
            </h1>
            <p class="hero-summary mt-4">
             Read all saved and downloaded books at <br> your convenience
            </p>

            <p class="mt-5">
            <a href="{{route('books')}}" class="btn btn-primary border-0" id="primary-bg"
                >View books</a
              >
              
            </p>
          </div>
          <div class="col-lg-6"></div>
        </div>
      


    
        <section class="top-picks bg-white">
          <div class="row mt-5 trending-row">
            <div class="col-lg-12 d-flex justify-content-between align-items-center" id="trending-heading">
              <h5 class="font-weight-bold">CONTINUE READING</h5>
            </div>
          </div>

          @if ($openedBooks->count() > 0)
              <div class="row mt-0 pt-0" id="book-master">
                @foreach ($openedBooks as $book)
                      <x-book :book="$book->book"/>
                @endforeach
              </div>

            @else 
              <div class="py-5 px-5">
                <x-empty-card />
              </div>
           @endif
        </section>
   



    @if ($completedBooks->count() > 0)
        <section class="top-picks bg-body mt-0">
          <div class="row mt-5 trending-row">
            <div class="col-lg-12 d-flex justify-content-between align-items-center" id="trending-heading">
              <h5 class="font-weight-bold">COMPLETED</h5>
            </div>
          </div>

          <div class="row mt-0 pt-0" id="book-master">
            @foreach ($completedBooks as $book)
                  <x-book :book="$book"/>
            @endforeach
          </div>
        </section>
    @endif

    @if ($downloadedBooks->count() > 0)
        <section class="top-picks bg-body mt-0">
          <div class="row mt-5 trending-row">
            <div class="col-lg-12 d-flex justify-content-between align-items-center" id="trending-heading">
              <h5 class="font-weight-bold">DOWNLOADED</h5>
            </div>
          </div>

          <div class="row mt-0 pt-0" id="book-master">
            @foreach ($downloadedBooks as $book)
                  <x-book :book="$book"/>
            @endforeach
          </div>
        </section>
    @endif

    </div>
@endsection