@extends('layouts.app')
@section('title', 'Profile')

@section('content')
    <div class="container-fluid">
      <div class="row">
        @include('_inc.profile-side')
        <div class="col-lg-10" id="edit-profile-box">
          <div class="bg-white rounded px-3 py-3 mt-4">
            <div class="row px-3 mt-5">
              <div class="col-lg-3">
                @livewire('photo-upload')
              </div>
              <div class="col-lg-9 mt-4 mt-md-0">
                <h5 class="font-weight-bold mb-4">Personal Information</h5>

                <form class="mt-4" method="POST" action="{{route('profile.update')}}">
                    @csrf
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for=""
                        ><small><b>First Name</b></small></label
                      >
                      <input type="text" name="firstname" value="{{auth()->user()->firstname}}" class="form-control" />
                      @error('firstname')
                        <small class="text-danger">{{$message}}</small>
                      @enderror
                    </div>

                    <div class="form-group col-md-6">
                      <label for=""
                        ><small><b>Last Name</b></small></label
                      >
                      <input type="text" name="lastname" value="{{auth()->user()->firstname}}" class="form-control"/>
                      @error('lastname')
                        <small class="text-danger">{{$message}}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for=""
                        ><small><b>Email Address</b></small></label
                      >
                      <input type="text" name="email" readonly value="{{auth()->user()->email}}" class="form-control" />
                      @error('email')
                        <small class="text-danger">{{$message}}</small>
                      @enderror
                    </div>

                    <div class="form-group col-md-6">
                      <label for=""
                        ><small><b>Phone Number</b></small></label
                      >
                      <input type="text"  name="phone" readonly value="{{auth()->user()->phone}}" class="form-control" />
                       @error('phone')
                        <small class="text-danger">{{$message}}</small>
                      @enderror
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for=""
                        ><small><b>Username</b></small></label
                      >
                      <input type="text" name="username" value="{{auth()->user()->username}}" class="form-control" />
                      @error('username')
                        <small class="text-danger">{{$message}}</small>
                      @enderror
                    </div>

                    <div class="form-group col-md-6">
                      <label for=""
                        ><small><b>Subscription Plan</b></small></label
                      >
                      <input type="text" readonly class="form-control" />
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for=""
                        ><small><b>Join Date</b></small></label
                      >
                      <input type="text" readonly value="{{now()->parse(auth()->user()->created_at)->format('d-m-Y')}}" class="form-control" class="form-control" />
                    </div>

                    {{-- <div class="form-group col-md-3 col-6">
                      <label for=""
                        ><small><b>Last Active</b></small></label
                      >
                      <input type="text" class="form-control" />
                    </div> --}}

                    <div class="form-group col-md-3 col-6">
                      <label for=""
                        ><small><b>Gender</b></small></label
                      >
                      <select name="gender" id="" class="form-control">
                        <option value="1" {{auth()->user()->is_male ? 'selected' : ''}}>Male</option>
                        <option value="0" {{!auth()->user()->is_male ? 'selected' : ''}}>Female</option>
                      </select>
                       @error('gender')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12">
                      <label for="">About</label>
                      <textarea
                        name="about"
                        class="form-control"
                        id=""
                        cols="30"
                        rows="4"
                      >{{auth()->user()->about}}</textarea>

                        @error('about')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="col-lg-12 mt-4 mb-5">
                      <button class="btn btn-success pl-5 pr-5 pt-2 pb-2" type="submit">
                        Submit
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection