@extends('layouts.app')
@section('title', 'Books')
@section('content')
   @livewire('books', ['books' => $books, 'genre' => isset($genre) ?  $genre : null ])
@endsection