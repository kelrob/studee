@extends('layouts.app')
@section('title', 'Read Book')
@section('content')

@livewire('read-book', ['book' => $book])
@endsection