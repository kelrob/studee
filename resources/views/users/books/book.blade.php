@extends('layouts.app')
@section('title', ucfirst($book->title))
@section('content')
<div class="my-0 bg-white book">
    <div class="container py-5">
        <div class="row py-4 align-items-center">
            <div class="col-md-3 my-3">
                <img src="{{$book->cover}}" alt="{{$book->name}}" class="img-fluid">
            </div>
            <div class="col-md-9 pl-5">
                <div class="text-left">
                    <h4 class=" font-weight-bolder">{{ucfirst($book->title)}}</h4>
                    <p>by {{ucfirst($book->author)}}</p>
                </div>
                <hr/>

                <div class="mt-4">
                    <h6 class="mb-2">About the Book</h6>
                    <div class="text-left mb-3">
                        <p>
                            {{$book->about}}
                        </p>
                    </div>
                    <div class="mb-5 d-flex align-items-center">
                        <x-rating />
                        <small>(123 reviews)</small>
                    </div>

                    <div class="row mb-5">
                        <div class="col-md-2 col-12 my-2 my-md-0 d-flex flex-md-column justify-content-md-center notations align-items-md-center align-items-start border-r-2 border-dark">
                            <span class="genre-text"><small>GENRE</small></span> <span class="d-block d-lg-none"> : &nbsp;</span>
                            <span class=" mt-0 mt-md-2">{{$book->genre ? ucfirst($book->genre->name) : '' }}</span>
                        </div>
                        <div class="col-md-2 col-12 my-2 my-md-0 d-flex flex-md-column justify-content-md-center notations align-items-md-center align-items-start border-r-2 border-dark">
                            <span class="genre-text"><small>PUBLISHED</small></span> <span class="d-block d-lg-none"> : &nbsp;</span>
                            <span class=" mt-0 mt-md-2">{{$book->published_at }}</span>
                        </div>
                        <div class="col-md-3 col-12 my-2 my-md-0 d-flex flex-md-column justify-content-md-center align-items-md-center align-items-start border-r-2 border-dark">
                            <span class="genre-text"><small>AUTHOR</small></span> <span class="d-block d-lg-none"> : &nbsp;</span>
                            <span class="mt-0 mt-md-2">{{ucfirst($book->author) }}</span>
                        </div>
                        {{-- <div class="col-md-2 col-12 my-2 my-md-0 d-flex flex-md-column justify-content-md-center notations justify-content-start border-r-2 border-dark">
                            <span class="genre-text">LENGTH</span> <span class="d-block d-lg-none"> :</span>
                            <span></span>
                        </div> --}}
                        {{-- <div class="col-md-2 col-12 my-2 my-md-0 d-flex flex-md-column justify-content-md-center notations justify-content-start">
                            <span class="genre-text">EDITION</span> <span class="d-block d-lg-none"> :</span>
                            <span></span>
                        </div> --}}
                        {{-- <div class="col-md-2 col-12 my-2 my-md-0 d-flex flex-md-column justify-content-md-center justify-content-start">
                            <span class="genre-text">APPROX TIME</span> <span class="d-block d-lg-none"> :</span>
                            <span></span>
                        </div> --}}
                    </div>

                    <div class="d-flex justify-content-start">
                        @if (!$inLibrary)
                            <a class="btn btn-outline-dark px-md-5 rounded-0" href="{{route('book.add', encrypt($book->id))}}">Add to Library</a>
                            {{-- @else
                            <a class="btn btn-outline-dark px-md-5 rounded-0" href="{{route('book.download', encrypt($book->id))}}">Download Book</a> --}}
                        @endif
                        <a class="btn btn-danger ml-3 px-md-5 rounded-0" href="{{route('book.read', encrypt($book->id))}}">Read now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection