@if ($book != null)
    <a href="{{route('book', encrypt($book->id))}}" class="col-6 col-md-3 link-unstyled">
        <img src="{{$book->cover}}" class="img-fluid" />
        <p class="bg-light p-2 mt-2 rounded font-weight-bold">
            {{truncateString(ucfirst($book->title), 24)}} <br />
            <small class="font-weight-light"
            ><i>By: {{ucfirst($book->author)}}</i></small
            >
        </p>
    </a>
@endif

