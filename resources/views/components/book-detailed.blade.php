<div class="col-12 mb-5">
    <div
        class="row bg-white mt-3 mt-lg-5 shadow-sm p-4 border-bottom"
        id="trending-content"
        >
        <div class="col-lg-3 text-lg-center text-left">
            <img src="{{$book->cover}}" id="trending-book" />
        </div>
        <div class="col-lg-9 mt-4">
            <h6 class="font-weight-bold">{{ucfirst($book->title)}}</h6>
            <p class="m-0 p-0">
            <small><i>By {{ucfirst($book->author)}}</i></small>
            </p>
            <p class="mt-4 summary-text">
            {{$book->about}}
            </p>
            <p class="mt-3">
            <a
                class="btn btn-primary border-0 pt-2 pb-2 btn-sm"
                id="primary-bg"
                href="{{route('book.read', encrypt($book->id))}}"
            >
                Read now
            </a>
            </p>
        </div>
    </div>
</div>