<div>
    @foreach ($genres as $genre)
        <li>
            <a href="{{route('books', $genre->name)}}">{{ucfirst($genre->name)}}</a>
        </li>
    @endforeach
    
</div>