<a href="{{ route('books', $genre->name) }}" class="col-lg-3 col-6 mb-4">

    <div class="item pl-0">
        <img src="{{ $genre->image }}" class="img-fluid" />
        <div class="mt--5 pt-1"
            style="margin-top: -12%; background-image: linear-gradient(to right, rgba(0,0,0,0), rgba(0,0,0,0.5)); position: relative;">
            <span class="w-100">
                <h5 class="font-weight-bold text-white" style="letter-spacing: 1.px;">
                    {{ truncateString(ucfirst($genre->name), 18) }}</h5>
            </span>
        </div>
    </div>

</a>
