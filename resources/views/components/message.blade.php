<div style="top: 0; margin-top: 10px; z-index: 1000;" class="w-100 d-flex position-fixed justify-content-end align-items-end flex-column">

    @if (session('success') || session('error'))
        <div @class([
            "alert alert-dismissible fade show",
            session('success') ? "alert-success" : "alert-danger" 
        ]) role="alert">
            <strong>{{session('success') ? 'Success': 'Error'}}!</strong> {{session('success') ? session('success') : session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <!-- Because you are alive, everything is possible. - Thich Nhat Hanh -->
</div>