<div class="card">
    <div
        class="card-header"
        id="headingFour"
        data-toggle="collapse"
        data-target="#collapseFour"
        aria-expanded="false"
        aria-controls="collapseThree"
    >
        <p class="mb-0 font-weight-bold">
        {{$faq->question}}
        <span class="float-right"><i class="fas fa-plus"></i></span>
        </p>
    </div>
    <div
        id="collapseFour"
        class="collapse"
        aria-labelledby="headingFour"
        data-parent="#accordion"
    >
        <div class="card-body">
            {{$faq->answer}}
        </div>
    </div>
</div>