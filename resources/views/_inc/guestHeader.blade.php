<div id="header">
    <div class="d-flex justify-content-between  flex-md-row align-items-center py-3 px-2 px-md-4 mb-1 bg-white">
        <h5 class="my-0 mr-md-auto font-weight-normal ml-lg-5 ml-3">
            <img src="/assets/images/logo-darks.png" id="logo" />
        </h5>

        @if (!Auth::check())
            <a class="btn btn-primary border-0 pl-3 pr-3 pt-1 pb-1 btn-sm mr-lg-5 mr-3" id="primary-bg"
                href="{{ route('login') }}">Sign in</a>
        @else
            <a class="btn btn-primary border-0 pl-3 pr-3 pt-1 pb-1 btn-sm mr-lg-5 mr-3" id="primary-bg"
                href="{{ url('dashboard') }}">Go to dashboard</a>
        @endif

    </div>
    <section id="hero-section"
        class="text-center d-flex d-xl-block flex-column align-items-center justify-content-center ">
        <div id="hero-header">
            <h1 class="font-weight-bold">
                Enjoy reading from <br />
                the comfort of your home
            </h1>
        </div>
        <div id="hero-body" class="mt-5">
            <p>
                Get all the academic knowledge you ever wished for on our site
            </p>

            <div class="row p-0 m-0">
                <div class="col-lg-12" align="center">
                    {{-- <form action=""> --}}
                    <form id="search-form" method="POST" action="{{route('books.search')}}" class="d-none d-xl-block">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="input" id="search-box"
                                placeholder="Search books, names of author" />
                            <div class="input-group-append">
                                <button class="btn btn-primary border-0 pl-3 pr-3" id="primary-bg" type="submit">
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>

                    <form id="search-form-sm" method="POST" action="{{route('books.search')}}">
                        @csrf
                        <div class="form-group mb-3 d-xl-none d-block">
                            <input type="text" class="form-control" name="input" id="search-box"
                                placeholder="Search books, names of author" />
                            <div class="input-group-append">
                                <button class="btn btn-primary border-0 btn-block pl-3 pr-3" id="primary-bg"
                                    type="submit">
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>
                    {{-- </form> --}}
                </div>
            </div>
        </div>
    </section>
</div>
