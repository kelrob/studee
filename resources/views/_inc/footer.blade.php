<footer id="footer">
    <div class="row">
        <div class="col-lg-3">
        <h6 class="font-weight-bold">
            About &nbsp; <img src="/assets/images/logo-darks.png" />
        </h6>

        <p class="mt-4">
            <small
            >Sign in to get access to all our books and get notifications
            when we post and upload new book</small
            >
        </p>
        </div>
        <div class="col-lg-3">
        <h6 class="font-weight-bold">Library</h6>

        <p class="mt-4"><small>Genres</small></p>
        <p class="mt-2"><small>Languages</small></p>
        <p class="mt-2"><small>Authors</small></p>
        </div>
        <div class="col-lg-3">
        <h6 class="font-weight-bold">Contact Us</h6>

        <p class="mt-4"><small>+44 345 678 903</small></p>
        <p class="mt-2"><small>hi@studee.com</small></p>
        <p class="mt-2">
            <small
            >Elon Musk Street, Lagos State Nigeria Lagos Island. Lekki
            Peninsula</small
            >
        </p>
        <p class="icons">
            <i class="fab fa-facebook-f"></i> &nbsp;
            <i class="fab fa-instagram"></i> &nbsp;
            <i class="fab fa-linkedin"></i> &nbsp;
            <i class="fab fa-twitter"></i> &nbsp;
        </p>
        </div>
        <div class="col-lg-3">
            <h6 class="font-weight-bold">
                Subscribe to &nbsp;
                <img src="./assets/images/logo-darks.png" /> via email
            </h6>

            <p class="mt-4">
                <small
                >Be the first to get information on our latest books and
                publishes any ongoing project.</small
                >
            </p>
            @livewire('news-letter', ['instance' => 'footer'])
        </div>
    </div>
    </footer>