<div class=" left-side-bar">
		<div class="brand-logo">
			<a href="{{route('admin.index')}}">
				<img src="/assets/images/logo-dark.png" style="height: 2rem;" alt="" class="dark-logo">
				<img src="/assets/images/logo.png" alt="" class="light-logo">
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="{{route('admin.index')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-house-1"></span><span class="mtext">Dashboard</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-user-12"></span><span class="mtext">Users</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.users')}}">All Users</a></li>
							<li><a href="{{route('admin.users', 'subscribed')}}">Subscribed Users</a></li>
							<li><a href="{{route('admin.users', 'unsubscribed')}}">Unsubscribed Users</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-library"></span><span class="mtext">Books</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.books')}}">All Books</a></li>
							<li><a href="{{route('admin.book.add')}}">Add Book</a></li>
						</ul>
					</li>

                    <li class="dropdown">
						<a href="{{route('admin.genres')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-grid"></span><span class="mtext">Genres</span>
						</a>
						{{-- <ul class="submenu">
							<li><a href="{{route('admin.genres')}}">All Genres</a></li>
							<li><a href="{{route('admin.genre')}}">Add Genre</a></li>
						</ul> --}}
					</li>
					<li>
						<a href="{{route('admin.plans')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-calendar-3"></span><span class="mtext">Subscription Plans</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>