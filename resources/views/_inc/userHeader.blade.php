<div x-data="{openSearch: false, openGenre: false}" @class([ 
  Route::is('dashboard') ? "hero pt-3" : "no-gutter",
  "position-relative"
])>
<nav @class([
  "navbar py-3 navbar-expand-sm navbar-dark navbar-dark pt-3 mb-3",
  !Route::is('dashboard') ? "bg-white mb-5" : "", "position-relative relative"
])>
  <div class="container">
    <a href="/" class="navbar-brand">
      @if (Route::is('dashboard'))
          <img src="/assets/images/logo.png" id="logo" />
      @else
          <img src="/assets/images/logo-dark.png" id="logo" />
      @endif
    </a>

    <div class="d-flex align-items-center">
      <button class="mr-1 btn btn-default p-0 d-md-none d-block" @click.prevent="openSearch = true" type="button">
        <span @class([
          Route::is('dashboard') ?  "text-white" : "text-dark"
        ])>
          <svg xmlns="http://www.w3.org/2000/svg" style="height: 1.8rem;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
          </svg>
        </span>
    </button>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
        <span @class([
          Route::is('dashboard') ?  "text-white" : "text-dark"
        ])>
          <svg xmlns="http://www.w3.org/2000/svg" style="height: 1.8rem;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h8m-8 6h16" />
          </svg>
        </span>
    </button>
    </div>


    <div id="navbarCollapse" class="collapse navbar-collapse py-2 py-md-0">
      <ul class="navbar-nav ml-auto">
        
          <li class="nav-item">
              <a @class([
                "p-2 mx-md-3 d-inline-block nav-link",
                Route::is('dashboard') ? "text-white" : 'text-dark'
              ]) id="nav-item" href="{{route('myLibrary')}}"
                >My Library</a
              >
          </li>
          <li class="nav-item">
              <a @class([
                "p-2 mx-md-3 d-inline-block nav-link",
                Route::is('dashboard') ? "text-white" : 'text-dark'
              ]) id="nav-item" href="{{route('books')}}">Books</a>
          </li>
          <li class="nav-item position-relative">
              <a @class([
                "p-2 mx-md-3 d-inline-block nav-link",
                Route::is('dashboard') ? "text-white" : 'text-dark'
              ]) id="nav-item" href="{{route('books')}}" @click.prevent="openGenre = true"
                  >Genres</a
                >
                <ul class="nav-dropdown pl-4 px-md-3" x-show="openGenre" @click.outside="openGenre = false">
                  <x-genre-dropdown />
                </ul>
          </li>
          
          @if (!auth()->user()->is_subscribed())
              <li class="nav-item">
                <a @class([
                  "p-2 mx-md-3 d-inline-block nav-link",
                  Route::is('dashboard') ? "text-white" : 'text-dark'
                ]) id="nav-item" href="{{route('subscription-plan')}}"
                  >Plans</a
                >
            </li>
          @endif
          <li class="nav-item d-none d-md-block">
              <a @class([
                "p-2 mx-md-3 d-inline-block nav-link",
                Route::is('dashboard') ? "text-white" : 'text-dark'
              ]) id="nav-item" @click.prevent="openSearch = true" href="#"
                ><span class="d-none d-xl-block">Search</span>
                <span class="d-xl-none d-block">
                  <svg xmlns="http://www.w3.org/2000/svg" style="height: 1.8rem;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                  </span></a
              >
          </li>
          <li class="nav-item d-md-none d-block">
              <a @class([
                "p-2 mx-md-3 d-inline-block nav-link",
                Route::is('dashboard') ? "text-white" : 'text-dark'
              ]) id="nav-item" href="{{route('profile')}}"
                >My Profile</a
              >
          </li>

          <li class="nav-item d-md-none d-block">
              <a @class([
                "p-2 mx-md-3 d-inline-block nav-link",
                Route::is('dashboard') ? "text-white" : 'text-dark'
              ]) id="nav-item" href="{{route('profile')}}"
                >Change Password</a
              >
          </li>

          <li class="nav-item d-md-none d-block">
              <a @class([
                "p-2 mx-md-3 d-inline-block nav-link",
                Route::is('dashboard') ? "text-white" : 'text-dark'
              ]) id="nav-item" href="{{route('logout')}}"
                >Logout</a
              >
          </li>
          <li class="nav-item d-none d-md-block">
            <a href="{{route('profile')}}" class="nav-link p-2 p-md-0 px-md-0 px-2">
              <img id="profile-icon" style="border-radius: 9999px; height: 40px;" src="{{auth()->user()->profile_photo_path == null ? 
                auth()->user()->getProfilePhotoUrlAttribute() : 
                Storage::url(auth()->user()->profile_photo_path) }}"/>
            </a>
          </li>
      </ul>
    </div>
  </div>
    <div class="bg-white search-container" @click.outside="openSearch = false" x-show="openSearch">
       <div class="d-flex justify-content-center flex-column align-items-center">
         <div id="search-form" class="w-100">
          <form action="{{route('books.search')}}" method="POST">
            @csrf
              <div class="input-group mb-3">
                <input type="text" name="input" x-on:keydown.escape="openSearch = false" autofocus class="form-control" id="search-box"
                  placeholder="Search books, names of author" />
                  <div class="input-group-append">
                      <button class="btn btn-primary border-0 pl-3 pr-3" id="primary-bg" type="submit">
                          Search
                      </button>
                  </div>
                </div>
            </form>
          </div>
       </div>
    </div>
</nav>


  @if (Route::is('dashboard'))
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h1 class="hero-header">
              Enjoy reading from <br />
              the comfort of your home
            </h1>
            <p class="hero-summary text-white mt-4">
              Get all the academic knowledge you ever need <br />
              By subscribing and reading our books online at <br />
              A lower cost.
            </p>

            <p class="mt-5">
              <a href="{{route('books')}}" class="btn btn-primary border-0" style="padding-left: 50px; padding-right: 50px;" id="primary-bg"
                >Read now</a
              >
              {{-- <a href="{{route('books')}}" class="btn btn-primary border-0" id="primary-bg"
                >Add to Library</a
              > --}}
            </p>
          </div>
          <div class="col-lg-6"></div>
        </div>
      </div>
      @elseif (Route::is('books'))
          {{-- <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <h1 class="hero-header text-white">
                  Get the best <br /> from our platform
                </h1>
                <p class="hero-summary text-light mt-4">
                Read all saved and downloaded books at <br> your convenience
                </p>

                <p class="mt-5">
                <a href="{{route('books')}}" class="btn btn-primary border-0" id="primary-bg"
                    >View books</a
                  >
                  
                </p>
              </div>
              <div class="col-lg-6"></div>
            </div>
          </div>
      <div class="overlay"></div> --}}
    @endif
</div>
