<div class="col-lg-2 side-bar bg-white p-2 d-none d-md-block">
    <ul>
    <li class="active">
        <a href="{{route('profile')}}"
        ><img src="/assets/images/icons/user.svg" />My Profile</a
        >
    </li>
    {{-- <li>
        <a href="update-profile.html"
        ><img src="/assets/images/icons/resume.svg" />Update Profile</a
        >
    </li> --}}
    <li>
        <a href="{{route('profile.password')}}"
        ><img src="/assets/images/icons/lock.svg" />Change Password</a
        >
    </li>
    {{-- <li>
        <a href="#"
        ><img src="/assets/images/icons/subscribe.svg" />Subscription
        Plan</a
        >
    </li> --}}
    <li>
        <a href="{{route('logout')}}"
        ><img src="/assets/images/icons/logout (1).svg" />Logout
        </a>
    </li>
    </ul>
</div>