<?php

use App\Http\Controllers\Admin\Books;
use App\Http\Controllers\Admin\Pages;
use App\Http\Controllers\Admin\Plans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OverwriteController;
use App\Http\Controllers\User\BooksController;
use App\Http\Controllers\User\PagesController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\SubscriptionController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'welcome'])->name('welcome');


Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect()->route('dashboard');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    session()->flash('status', 'verification-link-sent');
    return back();
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');





Route::middleware(['auth:sanctum'])->group(function () {

    Route::view('/logout', 'auth.logout')->name('logout');
    Route::middleware(['verified'])->group(function () {
        
        Route::get('/dashboard', [PagesController::class, 'dashboard'])->name('dashboard');
        Route::get('/my-library', [PagesController::class, 'myLibrary'])->name('myLibrary');
        Route::prefix('books')->group(function() {
            Route::get('all/{genre?}', [BooksController::class, 'index'])->name('books');
            Route::get('book/{book}', [BooksController::class, 'get'])->name('book');
            Route::post('/search', [BooksController::class, 'search'])->name('books.search');
            Route::get('/{book}/read', [BooksController::class, 'readBook'])->name('book.read')->middleware('is_subscribed');
            Route::get('/{book}/add-to-library', [BooksController::class, 'add'])->name('book.add');
            Route::get('/{book}/download', [BooksController::class, 'download'])->name('book.download');
        });

        Route::prefix('subscription')->group(function () {
            Route::get('plans', [SubscriptionController::class, 'plans'])->name('subscription-plan');
            Route::post('plan/subscribe', [SubscriptionController::class, 'choosePlan'])->name('subscribe');
        });

        Route::prefix('profile')->group(function () {
            Route::get('profile', [ProfileController::class, 'index'])->name('profile');
            Route::post('update', [ProfileController::class, 'updateProfile'])->name('profile.update');
            Route::post('password/update', [ProfileController::class, 'changePassword'])->name('profile.password.update');
            Route::get('password', [ProfileController::class, 'password'])->name('profile.password');
        });
    });


    Route::group(['prefix' => 'admin', 'middleware' => ['is_admin']],function () {
        Route::get('/', [Pages::class, 'index'])->name('admin.index');
        Route::get('users/{category?}', [Pages::class, 'users'])->name('admin.users');
        Route::get('/user/{user}', [Pages::class, 'user'])->name('admin.user');

        Route::group(['prefix' => 'subscriptions'],function () {
            Route::get('/plans', [Plans::class, 'index'])->name('admin.plans');
            Route::get('/{plan}/edit', [Plans::class,
            'edit'])->name('admin.plan.edit');
            Route::post('/edit/update', [Plans::class, 'updatePlan'])->name('admin.plan.update');
        });

        Route::group(['prefix' => 'books'], function (){
            Route::get('/genres', [Books::class, 'genres'])->name('admin.genres');
            Route::post('/genre/add', [Books::class, 'genre'])->name('admin.genre');
            Route::post('/genre/update', [Books::class,'updateGenre'])->name('admin.genre.update');
            Route::get('/genre/{genre_id}/edit', [Books::class, 'editGenre'])->name('admin.genre.edit');
            Route::get('/genre/{genre_id}/delete', [Books::class, 'deleteGenre'])->name('admin.genre.delete');
            Route::get('/', [Books::class, 'index'])->name('admin.books');
            Route::get('/add', [Books::class, 'add'])->name('admin.book.add');
            Route::post('/add/save', [Books::class, 'save'])->name('admin.book.save');
            Route::get('/{book_id}/edit', [Books::class, 'edit'])->name('admin.book.edit');
            Route::post('/book/update', [Books::class, 'update'])->name('admin.book.update');
        });
    });

    Route::get('/payment/callback', [SubscriptionController::class, 'callback'])->name('payment-callback');
});

